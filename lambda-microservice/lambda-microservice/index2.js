'use strict'

const mysql = require('mysql');
const config = require("./config.json");

const connection = mysql.createConnection({
    host: config.dbhost,
    database: config.dbname,
    user: config.dbuser,
    password: config.dbpassword
});

exports.handler = function (event, context, callback) {
    console.log(JSON.stringify(`Event: event`))

    // context.succeed('Success!')
    // context.fail('Failed!')
}