console.log('Loading function');

//const doc = require('dynamodb-doc');
//const dynamo = new doc.DynamoDB();

const mysql = require('mysql');
const config = require("./config.json");

const connection = mysql.createConnection({
    host: config.dbhost,
    database: config.dbname,
    user: config.dbuser,
    password: config.dbpassword
});

//function create_UUID(){
//    var dt = new Date().getTime();
//    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
//        var r = (dt + Math.random()*16)%16 | 0;
//        dt = Math.floor(dt/16);
//        return (c == 'x' ? r :(r&0x3|0x8)).toString(16);
//    });
//    return uuid;
//}

exports.handler = (event, context, callback) => {
    console.log('Received event:', JSON.stringify(event, null, 2));
    context.callbackWaitsForEmptyEventLoop = false;

    const done = (err, res) => callback(null, {
        statusCode: err ? '400' : '200',
        body: err ? err.message : JSON.stringify(res),
        headers: { 'Content-Type': 'application/json' }
    });

    connection.query('show tables', function (error, results, fields) {
        connection.end();
        if (error) {
            throw error;
        } else {
            console.log(results);
            //callback(error, results);
            //connection.end(function (err) { callback(err, results); });
        }
    });

/*
    switch (event.httpMethod) {
        case 'DELETE':
            dynamo.deleteItem(JSON.parse(event.body), done);
            break;
        case 'GET':
            dynamo.scan({ TableName: event.queryStringParameters.TableName }, done);
            break;

        case 'POST':
            var params = {};
            params.TableName = "quoreGateway";
            params.Item = { "eventId": create_UUID(), "event": event.body };

            dynamo.putItem(params, done);
            break;

        case 'PUT':
            dynamo.updateItem(JSON.parse(event.body), done);
            break;

        default:
            done(new Error(`Unsupported method "${event.httpMethod}"`));
    }
*/
};
