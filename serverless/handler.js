'use strict';

const mysql = require('mysql');
const config = require("./config.json");

const connection = mysql.createConnection({
    host: config.dbhost,
    database: config.dbname,
    user: config.dbuser,
    password: config.dbpassword
});
//console.log(connection);

module.exports.hello = async (event) => {
  await connection.query('show tables', function (error, results, fields) {
    connection.end();
    return {
      statusCode: 200,
      body: results
    };
  });


  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
};
