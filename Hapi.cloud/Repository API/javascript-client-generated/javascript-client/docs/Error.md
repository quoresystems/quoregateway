# HapiRepositoryApi.Error

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**timestamp** | **Number** |  | 
**status** | **Number** |  | 
**error** | **String** |  | 
**exception** | **String** |  | [optional] 
**message** | **String** |  | 
**path** | **String** |  | [optional] 


