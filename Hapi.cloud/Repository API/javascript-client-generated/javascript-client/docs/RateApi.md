# HapiRepositoryApi.RateApi

All URIs are relative to *https://hapicloud-dev.apigee.net/repository*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getRate**](RateApi.md#getRate) | **GET** /rates/{propertyCode}/{rateCode} | Get rate by rateCode
[**getRates**](RateApi.md#getRates) | **POST** /rates/search | Get rates by date range. Paging is included. Maximum value of returned rates is 1000


<a name="getRate"></a>
# **getRate**
> Rate getRate(propertyCode, rateCode)

Get rate by rateCode

### Example
```javascript
var HapiRepositoryApi = require('hapi_repository_api');
var defaultClient = HapiRepositoryApi.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new HapiRepositoryApi.RateApi();

var propertyCode = "propertyCode_example"; // String | Property code as designated in the PMS

var rateCode = "rateCode_example"; // String | Rate code


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getRate(propertyCode, rateCode, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **propertyCode** | **String**| Property code as designated in the PMS | 
 **rateCode** | **String**| Rate code | 

### Return type

[**Rate**](Rate.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getRates"></a>
# **getRates**
> InlineResponse200 getRates(searchRequest)

Get rates by date range. Paging is included. Maximum value of returned rates is 1000

### Example
```javascript
var HapiRepositoryApi = require('hapi_repository_api');
var defaultClient = HapiRepositoryApi.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new HapiRepositoryApi.RateApi();

var searchRequest = new HapiRepositoryApi.SearchRequest(); // SearchRequest | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getRates(searchRequest, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **searchRequest** | [**SearchRequest**](SearchRequest.md)|  | 

### Return type

[**InlineResponse200**](InlineResponse200.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

