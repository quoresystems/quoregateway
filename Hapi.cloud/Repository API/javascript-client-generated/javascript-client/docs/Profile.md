# HapiRepositoryApi.Profile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**propertyId** | [**PropertyId**](PropertyId.md) |  | [optional] 
**id** | **String** | reservation identifier | [optional] 
**type** | **String** | type of profile | [optional] 
**name** | [**Name**](Name.md) |  | [optional] 
**company** | **String** | used to provide company name when name field is used for contact info | [optional] 
**dateOfBirth** | **String** |  | [optional] 
**emails** | [**[Email]**](Email.md) |  | [optional] 
**phones** | [**[Phone]**](Phone.md) |  | [optional] 
**addresses** | [**[Address]**](Address.md) |  | [optional] 
**idDocuments** | [**[IdDocument]**](IdDocument.md) |  | [optional] 
**loyaltyPrograms** | [**[LoyaltyProgram]**](LoyaltyProgram.md) |  | [optional] 
**creator** | **String** | user or interface who created the profile | [optional] 
**createdDate** | **String** | read-only date and time the profile was created | [optional] 
**travelAgentId** | **String** | IATA number | [optional] 
**repositoryCreated** | **String** | repository create date in YYYY-MM-DDThh:mm:ss.sss format | [optional] 
**repositoryUpdated** | **String** | repository update date in YYYY-MM-DDThh:mm:ss.sss format | [optional] 


<a name="TypeEnum"></a>
## Enum: TypeEnum


* `PERSON` (value: `"PERSON"`)

* `COMPANY` (value: `"COMPANY"`)

* `TRAVEL_AGENT` (value: `"TRAVEL_AGENT"`)

* `GROUP_PROFILE` (value: `"GROUP_PROFILE"`)

* `OTHER` (value: `"OTHER"`)




