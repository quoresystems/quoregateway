# HapiRepositoryApi.Revenue

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **String** | classification of revenue indicated | [optional] 
**currencyCode** | **String** | currency code of revenue amount indicated | [optional] 
**amount** | **Number** | rate amount | [optional] 
**_date** | **String** | transaction date | [optional] 


