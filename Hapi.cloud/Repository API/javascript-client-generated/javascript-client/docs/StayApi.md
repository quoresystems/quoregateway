# HapiRepositoryApi.StayApi

All URIs are relative to *https://hapicloud-dev.apigee.net/repository*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getStays**](StayApi.md#getStays) | **GET** /stays/{propertyCode}/{reservationId} | Get stays by reservation number
[**searchStays**](StayApi.md#searchStays) | **POST** /stays/search | Get stays by date range. Paging is included. Maximum value of returned stays is 1000


<a name="getStays"></a>
# **getStays**
> Stay getStays(propertyCode, reservationId)

Get stays by reservation number

### Example
```javascript
var HapiRepositoryApi = require('hapi_repository_api');
var defaultClient = HapiRepositoryApi.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new HapiRepositoryApi.StayApi();

var propertyCode = "propertyCode_example"; // String | Property code as designated in the PMS

var reservationId = "reservationId_example"; // String | PMS Reservation id


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getStays(propertyCode, reservationId, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **propertyCode** | **String**| Property code as designated in the PMS | 
 **reservationId** | **String**| PMS Reservation id | 

### Return type

[**Stay**](Stay.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="searchStays"></a>
# **searchStays**
> InlineResponse200 searchStays(searchRequest)

Get stays by date range. Paging is included. Maximum value of returned stays is 1000

### Example
```javascript
var HapiRepositoryApi = require('hapi_repository_api');
var defaultClient = HapiRepositoryApi.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new HapiRepositoryApi.StayApi();

var searchRequest = new HapiRepositoryApi.SearchRequest(); // SearchRequest | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.searchStays(searchRequest, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **searchRequest** | [**SearchRequest**](SearchRequest.md)|  | 

### Return type

[**InlineResponse200**](InlineResponse200.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

