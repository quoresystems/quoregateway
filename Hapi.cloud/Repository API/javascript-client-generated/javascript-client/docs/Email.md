# HapiRepositoryApi.Email

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **String** | email type | [optional] 
**address** | **String** |  | [optional] 


<a name="TypeEnum"></a>
## Enum: TypeEnum


* `PRIVATE` (value: `"PRIVATE"`)

* `CORPORATE` (value: `"CORPORATE"`)

* `OTHER` (value: `"OTHER"`)




