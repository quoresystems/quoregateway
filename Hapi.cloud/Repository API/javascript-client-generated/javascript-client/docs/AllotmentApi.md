# HapiRepositoryApi.AllotmentApi

All URIs are relative to *https://hapicloud-dev.apigee.net/repository*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getAllotment**](AllotmentApi.md#getAllotment) | **GET** /allotments/{propertyCode}/{id} | Get allotments of specified hotel that defined by chainCode / propertyCode (HAPI internal property identifier) and by blockCode and effectiveDate (Allotment external identifier)
[**searchAllotments**](AllotmentApi.md#searchAllotments) | **POST** /allotments/search/{strategy} | Search allotments by date range. Paging is included. Maximum value of returned allotments is 1000


<a name="getAllotment"></a>
# **getAllotment**
> Allotment getAllotment(propertyCode, id)

Get allotments of specified hotel that defined by chainCode / propertyCode (HAPI internal property identifier) and by blockCode and effectiveDate (Allotment external identifier)

### Example
```javascript
var HapiRepositoryApi = require('hapi_repository_api');
var defaultClient = HapiRepositoryApi.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new HapiRepositoryApi.AllotmentApi();

var propertyCode = "propertyCode_example"; // String | Property code as designated in the PMS

var id = "id_example"; // String | Identifier of allotment record in the external system


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getAllotment(propertyCode, id, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **propertyCode** | **String**| Property code as designated in the PMS | 
 **id** | **String**| Identifier of allotment record in the external system | 

### Return type

[**Allotment**](Allotment.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="searchAllotments"></a>
# **searchAllotments**
> InlineResponse200 searchAllotments(strategy, searchRequest)

Search allotments by date range. Paging is included. Maximum value of returned allotments is 1000

### Example
```javascript
var HapiRepositoryApi = require('hapi_repository_api');
var defaultClient = HapiRepositoryApi.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new HapiRepositoryApi.AllotmentApi();

var strategy = "strategy_example"; // String | Type of the search strategy. Could be range or update.<br>In a case of range strategy records will be searched by their effectiveDate / expireDate instead of searching by updated field in a case of update strategy

var searchRequest = new HapiRepositoryApi.SearchRequest(); // SearchRequest | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.searchAllotments(strategy, searchRequest, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **strategy** | **String**| Type of the search strategy. Could be range or update.&lt;br&gt;In a case of range strategy records will be searched by their effectiveDate / expireDate instead of searching by updated field in a case of update strategy | 
 **searchRequest** | [**SearchRequest**](SearchRequest.md)|  | 

### Return type

[**InlineResponse200**](InlineResponse200.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

