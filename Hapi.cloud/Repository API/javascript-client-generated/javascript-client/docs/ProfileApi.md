# HapiRepositoryApi.ProfileApi

All URIs are relative to *https://hapicloud-dev.apigee.net/repository*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getProfile**](ProfileApi.md#getProfile) | **GET** /profiles/{propertyCode}/{id} | Get profile by id
[**getProfiles**](ProfileApi.md#getProfiles) | **POST** /profiles/search | Get profiles by date range. Paging is included. Maximum value of returned profiles is 1000


<a name="getProfile"></a>
# **getProfile**
> Profile getProfile(propertyCode, id)

Get profile by id

### Example
```javascript
var HapiRepositoryApi = require('hapi_repository_api');
var defaultClient = HapiRepositoryApi.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new HapiRepositoryApi.ProfileApi();

var propertyCode = "propertyCode_example"; // String | Property code as designated in the PMS

var id = "id_example"; // String | PMS profile id


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getProfile(propertyCode, id, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **propertyCode** | **String**| Property code as designated in the PMS | 
 **id** | **String**| PMS profile id | 

### Return type

[**Profile**](Profile.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getProfiles"></a>
# **getProfiles**
> InlineResponse200 getProfiles(searchRequest)

Get profiles by date range. Paging is included. Maximum value of returned profiles is 1000

### Example
```javascript
var HapiRepositoryApi = require('hapi_repository_api');
var defaultClient = HapiRepositoryApi.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new HapiRepositoryApi.ProfileApi();

var searchRequest = new HapiRepositoryApi.SearchRequest(); // SearchRequest | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getProfiles(searchRequest, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **searchRequest** | [**SearchRequest**](SearchRequest.md)|  | 

### Return type

[**InlineResponse200**](InlineResponse200.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

