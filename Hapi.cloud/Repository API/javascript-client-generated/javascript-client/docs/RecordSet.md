# HapiRepositoryApi.RecordSet

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total** | **Number** |  | [optional] 
**offset** | **Number** |  | [optional] 
**entities** | **[Object]** |  | [optional] 


