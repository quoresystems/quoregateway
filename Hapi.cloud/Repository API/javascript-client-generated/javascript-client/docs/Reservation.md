# HapiRepositoryApi.Reservation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**propertyId** | [**PropertyId**](PropertyId.md) |  | [optional] 
**id** | **String** | reservation identifier and object key | [optional] 
**bookingConfirmationId** | **String** | unique confirmation id within given hotel | [optional] 
**arrivalDate** | **String** | check-in date, in YYYY-MM-DD format | [optional] 
**arrivalTime** | **String** | estimated check-in time | [optional] 
**departureDate** | **String** | check-out date, in YYYY-MM-DD format | [optional] 
**creator** | **String** | user or interface who created the reservation | [optional] 
**createdDate** | **String** | read-only reservation create date in hotel, in YYYY-MM-DDThh:mm:ss.sss format | [optional] 
**cancelledDate** | **String** | Reservation cancellation date in hotel, in YYYY-MM-DDThh:mm:ss.sss format | [optional] 
**cancellationNumber** | **String** | cancellation number for reservation. may be supplied externally or returned from pms | [optional] 
**adults** | **Number** |  | [optional] 
**children** | **Number** |  | [optional] 
**status** | **String** | current status of reservation | [optional] 
**guests** | [**[Guest]**](Guest.md) |  | [optional] 
**profiles** | [**[Profile]**](Profile.md) |  | [optional] 
**roomStays** | [**[RoomStay]**](RoomStay.md) |  | [optional] 
**comments** | [**[Comment]**](Comment.md) |  | [optional] 
**repositoryCreated** | **String** | repository create date in YYYY-MM-DDThh:mm:ss.sss format | [optional] 
**repositoryUpdated** | **String** | repository update date in YYYY-MM-DDThh:mm:ss.sss format | [optional] 


<a name="StatusEnum"></a>
## Enum: StatusEnum


* `REQUESTED` (value: `"REQUESTED"`)

* `RESERVED` (value: `"RESERVED"`)

* `IN_HOUSE` (value: `"IN_HOUSE"`)

* `CANCELLED` (value: `"CANCELLED"`)

* `CHECKED_OUT` (value: `"CHECKED_OUT"`)

* `NO_SHOW` (value: `"NO_SHOW"`)

* `WAIT_LIST` (value: `"WAIT_LIST"`)

* `UNKNOWN` (value: `"UNKNOWN"`)




