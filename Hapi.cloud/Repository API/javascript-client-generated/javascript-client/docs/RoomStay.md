# HapiRepositoryApi.RoomStay

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**roomNumber** | **String** | room number for stay | [optional] 
**status** | **String** | current status of reservation | [optional] 
**guestId** | **String** | id of guest associated to stay | [optional] 
**roomType** | **String** | code identifying the type of room associated to stay | [optional] 
**rateCode** | **String** | code identifying rate for stay | [optional] 
**marketCode** | **String** | code identifying marketing type for rate | [optional] 
**channelCode** | **String** | code identifying source of reservation | [optional] 
**sourceCode** | **String** | code identifying source of rate associated to stay | [optional] 
**blockCode** | **String** | group block code | [optional] 
**roomCount** | **Number** | number of rooms associated to stay | [optional] 
**paymentMethod** | **String** | payment method used in the reservation | [optional] 
**compYN** | **String** | is reservation complimentary | [optional] 
**rates** | [**[RoomRate]**](RoomRate.md) |  | [optional] 
**revenue** | [**[Revenue]**](Revenue.md) |  | [optional] 
**services** | [**[Service]**](Service.md) |  | [optional] 


<a name="StatusEnum"></a>
## Enum: StatusEnum


* `REQUESTED` (value: `"REQUESTED"`)

* `RESERVED` (value: `"RESERVED"`)

* `IN_HOUSE` (value: `"IN_HOUSE"`)

* `CANCELLED` (value: `"CANCELLED"`)

* `CHECKED_OUT` (value: `"CHECKED_OUT"`)

* `NO_SHOW` (value: `"NO_SHOW"`)

* `WAIT_LIST` (value: `"WAIT_LIST"`)

* `UNKNOWN` (value: `"UNKNOWN"`)




