# HapiRepositoryApi.BlockRate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**guests** | **Number** | number of guests | [optional] 
**occupancyProjected** | **Number** | projected occupancy for number of guests indicated | [optional] 
**amount** | **Number** | rate amount | [optional] 
**includesTax** | **Boolean** | is indicated rate inclusive of tax | [optional] 


