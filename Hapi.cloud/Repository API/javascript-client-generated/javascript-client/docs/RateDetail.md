# HapiRepositoryApi.RateDetail

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dates** | [**TimeSpan**](TimeSpan.md) |  | [optional] 
**rateCode** | **String** | detail rate code | [optional] 
**marketCode** | **String** | rate marketing code | [optional] 
**sourceCode** | **String** | rate source code | [optional] 
**packages** | **String** | packages included in rate | [optional] 
**roomTypes** | **[String]** | valid room types for rate | [optional] 
**daysOfWeek** | **[String]** | Days of the week for which the rate is valid | [optional] 
**rates** | [**[RateInfo]**](RateInfo.md) | Rate amounts by number of guests and guest type | [optional] 


