# HapiRepositoryApi.RateInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**guests** | **Number** | rate occupancy | [optional] 
**guestType** | **String** | rate occupancy type | [optional] 
**amount** | **Number** | amount of rate | [optional] 


