# HapiRepositoryApi.IdDocument

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **String** |  | [optional] 
**_number** | **String** |  | [optional] 


