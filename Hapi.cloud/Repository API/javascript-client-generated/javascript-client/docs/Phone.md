# HapiRepositoryApi.Phone

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **String** | phone type | [optional] 
**_number** | **String** |  | [optional] 


<a name="TypeEnum"></a>
## Enum: TypeEnum


* `HOME` (value: `"HOME"`)

* `BUSINESS` (value: `"BUSINESS"`)

* `MOBILE` (value: `"MOBILE"`)

* `FAX` (value: `"FAX"`)

* `OTHER` (value: `"OTHER"`)




