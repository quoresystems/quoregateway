/**
 * HAPI Repository API
 * API for retrieving objects from HAPI repository
 *
 * OpenAPI spec version: 2
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.2
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', '../../src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require('../../src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.HapiRepositoryApi);
  }
}(this, function(expect, HapiRepositoryApi) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new HapiRepositoryApi.Name();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('Name', function() {
    it('should create an instance of Name', function() {
      // uncomment below and update the code to test Name
      //var instance = new HapiRepositoryApi.Name();
      //expect(instance).to.be.a(HapiRepositoryApi.Name);
    });

    it('should have the property salutation (base name: "salutation")', function() {
      // uncomment below and update the code to test the property salutation
      //var instance = new HapiRepositoryApi.Name();
      //expect(instance).to.be();
    });

    it('should have the property first (base name: "first")', function() {
      // uncomment below and update the code to test the property first
      //var instance = new HapiRepositoryApi.Name();
      //expect(instance).to.be();
    });

    it('should have the property middle (base name: "middle")', function() {
      // uncomment below and update the code to test the property middle
      //var instance = new HapiRepositoryApi.Name();
      //expect(instance).to.be();
    });

    it('should have the property last (base name: "last")', function() {
      // uncomment below and update the code to test the property last
      //var instance = new HapiRepositoryApi.Name();
      //expect(instance).to.be();
    });

  });

}));
