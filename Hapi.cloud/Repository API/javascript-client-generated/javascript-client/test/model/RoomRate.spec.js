/**
 * HAPI Repository API
 * API for retrieving objects from HAPI repository
 *
 * OpenAPI spec version: 2
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.2
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', '../../src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require('../../src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.HapiRepositoryApi);
  }
}(this, function(expect, HapiRepositoryApi) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new HapiRepositoryApi.RoomRate();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('RoomRate', function() {
    it('should create an instance of RoomRate', function() {
      // uncomment below and update the code to test RoomRate
      //var instance = new HapiRepositoryApi.RoomRate();
      //expect(instance).to.be.a(HapiRepositoryApi.RoomRate);
    });

    it('should have the property effectiveDates (base name: "effectiveDates")', function() {
      // uncomment below and update the code to test the property effectiveDates
      //var instance = new HapiRepositoryApi.RoomRate();
      //expect(instance).to.be();
    });

    it('should have the property rateCode (base name: "rateCode")', function() {
      // uncomment below and update the code to test the property rateCode
      //var instance = new HapiRepositoryApi.RoomRate();
      //expect(instance).to.be();
    });

    it('should have the property currencyCode (base name: "currencyCode")', function() {
      // uncomment below and update the code to test the property currencyCode
      //var instance = new HapiRepositoryApi.RoomRate();
      //expect(instance).to.be();
    });

    it('should have the property amount (base name: "amount")', function() {
      // uncomment below and update the code to test the property amount
      //var instance = new HapiRepositoryApi.RoomRate();
      //expect(instance).to.be();
    });

  });

}));
