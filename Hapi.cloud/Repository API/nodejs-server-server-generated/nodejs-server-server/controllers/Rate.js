'use strict';

var utils = require('../utils/writer.js');
var Rate = require('../service/RateService');

module.exports.getRate = function getRate (req, res, next) {
  var propertyCode = req.swagger.params['propertyCode'].value;
  var rateCode = req.swagger.params['rateCode'].value;
  Rate.getRate(propertyCode,rateCode)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getRates = function getRates (req, res, next) {
  var searchRequest = req.swagger.params['SearchRequest'].value;
  Rate.getRates(searchRequest)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
