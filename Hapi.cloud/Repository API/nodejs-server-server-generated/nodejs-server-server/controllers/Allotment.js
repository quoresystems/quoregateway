'use strict';

var utils = require('../utils/writer.js');
var Allotment = require('../service/AllotmentService');

module.exports.getAllotment = function getAllotment (req, res, next) {
  var propertyCode = req.swagger.params['propertyCode'].value;
  var id = req.swagger.params['id'].value;
  Allotment.getAllotment(propertyCode,id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.searchAllotments = function searchAllotments (req, res, next) {
  var strategy = req.swagger.params['strategy'].value;
  var searchRequest = req.swagger.params['SearchRequest'].value;
  Allotment.searchAllotments(strategy,searchRequest)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
