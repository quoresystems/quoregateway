'use strict';

var utils = require('../utils/writer.js');
var Stay = require('../service/StayService');

module.exports.getStays = function getStays (req, res, next) {
  var propertyCode = req.swagger.params['propertyCode'].value;
  var reservationId = req.swagger.params['reservationId'].value;
  Stay.getStays(propertyCode,reservationId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.searchStays = function searchStays (req, res, next) {
  var searchRequest = req.swagger.params['SearchRequest'].value;
  Stay.searchStays(searchRequest)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
