'use strict';

var utils = require('../utils/writer.js');
var Profile = require('../service/ProfileService');

module.exports.getProfile = function getProfile (req, res, next) {
  var propertyCode = req.swagger.params['propertyCode'].value;
  var id = req.swagger.params['id'].value;
  Profile.getProfile(propertyCode,id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getProfiles = function getProfiles (req, res, next) {
  var searchRequest = req.swagger.params['SearchRequest'].value;
  Profile.getProfiles(searchRequest)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
