'use strict';

var utils = require('../utils/writer.js');
var Reservations = require('../service/ReservationsService');

module.exports.getReservation = function getReservation (req, res, next) {
  var propertyCode = req.swagger.params['propertyCode'].value;
  var id = req.swagger.params['id'].value;
  Reservations.getReservation(propertyCode,id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getReservations = function getReservations (req, res, next) {
  var searchRequest = req.swagger.params['SearchRequest'].value;
  Reservations.getReservations(searchRequest)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
