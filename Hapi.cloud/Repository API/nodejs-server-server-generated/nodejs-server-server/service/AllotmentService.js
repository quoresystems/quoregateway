'use strict';


/**
 * Get allotments of specified hotel that defined by chainCode / propertyCode (HAPI internal property identifier) and by blockCode and effectiveDate (Allotment external identifier)
 *
 * propertyCode String Property code as designated in the PMS
 * id String Identifier of allotment record in the external system
 * returns Allotment
 **/
exports.getAllotment = function(propertyCode,id) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "includesFoodAndBev" : true,
  "repositoryUpdated" : "2018-06-01T15:34:54.000",
  "comments" : [ {
    "comment" : "0",
    "type" : "COMMISSION"
  }, {
    "comment" : "0",
    "type" : "COMMISSION"
  } ],
  "repositoryCreated" : "2018-06-01T15:34:54.000",
  "blockName" : "CONFERENCE HOLIDAY 2018",
  "blockType" : "ELASTIC",
  "profiles" : [ {
    "addresses" : [ {
      "country" : {
        "code" : "US",
        "name" : "United States"
      },
      "address3" : "address3",
      "address2" : "APT 505",
      "city" : "Miami",
      "address1" : "1234 Park Ave",
      "postalCode" : "33133",
      "stateProvince" : {
        "code" : "FL",
        "name" : "Florida"
      },
      "type" : "BUSINESS",
      "primary" : true
    }, {
      "country" : {
        "code" : "US",
        "name" : "United States"
      },
      "address3" : "address3",
      "address2" : "APT 505",
      "city" : "Miami",
      "address1" : "1234 Park Ave",
      "postalCode" : "33133",
      "stateProvince" : {
        "code" : "FL",
        "name" : "Florida"
      },
      "type" : "BUSINESS",
      "primary" : true
    } ],
    "creator" : "AFESSE",
    "repositoryUpdated" : "2018-06-01T15:34:54.000",
    "repositoryCreated" : "2018-06-01T15:34:54.000",
    "phones" : [ {
      "number" : "555-555-1212",
      "type" : "MOBILE"
    }, {
      "number" : "555-555-1212",
      "type" : "MOBILE"
    } ],
    "dateOfBirth" : "1979-10-31",
    "type" : "PERSON",
    "emails" : [ {
      "address" : "cguest@email.com",
      "type" : "PRIVATE"
    }, {
      "address" : "cguest@email.com",
      "type" : "PRIVATE"
    } ],
    "travelAgentId" : "ABC1234",
    "idDocuments" : [ {
      "number" : "12345",
      "type" : "PASSPORT"
    }, {
      "number" : "12345",
      "type" : "PASSPORT"
    } ],
    "createdDate" : "2018-05-14T20:02:40.000",
    "loyaltyPrograms" : [ {
      "membershipId" : "HR6585421",
      "type" : "Hotel Rewards"
    }, {
      "membershipId" : "HR6585421",
      "type" : "Hotel Rewards"
    } ],
    "name" : {
      "middle" : "M",
      "last" : "Guest",
      "salutation" : "Mr.",
      "first" : "Christopher"
    },
    "company" : "HAPI",
    "id" : "542154",
    "propertyId" : {
      "propertyCode" : "FSDH",
      "chainCode" : "HIL",
      "brandCode" : "ABVI"
    }
  }, {
    "addresses" : [ {
      "country" : {
        "code" : "US",
        "name" : "United States"
      },
      "address3" : "address3",
      "address2" : "APT 505",
      "city" : "Miami",
      "address1" : "1234 Park Ave",
      "postalCode" : "33133",
      "stateProvince" : {
        "code" : "FL",
        "name" : "Florida"
      },
      "type" : "BUSINESS",
      "primary" : true
    }, {
      "country" : {
        "code" : "US",
        "name" : "United States"
      },
      "address3" : "address3",
      "address2" : "APT 505",
      "city" : "Miami",
      "address1" : "1234 Park Ave",
      "postalCode" : "33133",
      "stateProvince" : {
        "code" : "FL",
        "name" : "Florida"
      },
      "type" : "BUSINESS",
      "primary" : true
    } ],
    "creator" : "AFESSE",
    "repositoryUpdated" : "2018-06-01T15:34:54.000",
    "repositoryCreated" : "2018-06-01T15:34:54.000",
    "phones" : [ {
      "number" : "555-555-1212",
      "type" : "MOBILE"
    }, {
      "number" : "555-555-1212",
      "type" : "MOBILE"
    } ],
    "dateOfBirth" : "1979-10-31",
    "type" : "PERSON",
    "emails" : [ {
      "address" : "cguest@email.com",
      "type" : "PRIVATE"
    }, {
      "address" : "cguest@email.com",
      "type" : "PRIVATE"
    } ],
    "travelAgentId" : "ABC1234",
    "idDocuments" : [ {
      "number" : "12345",
      "type" : "PASSPORT"
    }, {
      "number" : "12345",
      "type" : "PASSPORT"
    } ],
    "createdDate" : "2018-05-14T20:02:40.000",
    "loyaltyPrograms" : [ {
      "membershipId" : "HR6585421",
      "type" : "Hotel Rewards"
    }, {
      "membershipId" : "HR6585421",
      "type" : "Hotel Rewards"
    } ],
    "name" : {
      "middle" : "M",
      "last" : "Guest",
      "salutation" : "Mr.",
      "first" : "Christopher"
    },
    "company" : "HAPI",
    "id" : "542154",
    "propertyId" : {
      "propertyCode" : "FSDH",
      "chainCode" : "HIL",
      "brandCode" : "ABVI"
    }
  } ],
  "dates" : {
    "start" : "2018-12-01",
    "end" : "2018-12-03"
  },
  "bookingMethod" : "IWEB",
  "sourceCode" : "SRC",
  "billingType" : "billingType",
  "marketCode" : "F",
  "id" : "HOLIDAY2018",
  "propertyId" : {
    "propertyCode" : "FSDH",
    "chainCode" : "HIL",
    "brandCode" : "ABVI"
  },
  "inventoryBlocks" : [ {
    "numberSold" : 5,
    "numberToBlock" : 2,
    "compRoomsNumber" : 0,
    "rates" : [ {
      "amount" : 105.5,
      "occupancyProjected" : 0,
      "includesTax" : true,
      "guests" : 0
    }, {
      "amount" : 105.5,
      "occupancyProjected" : 0,
      "includesTax" : true,
      "guests" : 0
    } ],
    "dates" : {
      "start" : "2018-12-01",
      "end" : "2018-12-03"
    },
    "currencyCode" : "USD",
    "roomType" : "KING",
    "rateCode" : "AAA",
    "cutoffDate" : "2018-12-01"
  }, {
    "numberSold" : 5,
    "numberToBlock" : 2,
    "compRoomsNumber" : 0,
    "rates" : [ {
      "amount" : 105.5,
      "occupancyProjected" : 0,
      "includesTax" : true,
      "guests" : 0
    }, {
      "amount" : 105.5,
      "occupancyProjected" : 0,
      "includesTax" : true,
      "guests" : 0
    } ],
    "dates" : {
      "start" : "2018-12-01",
      "end" : "2018-12-03"
    },
    "currencyCode" : "USD",
    "roomType" : "KING",
    "rateCode" : "AAA",
    "cutoffDate" : "2018-12-01"
  } ],
  "status" : "ACTIVE",
  "channelCode" : "SRC"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Search allotments by date range. Paging is included. Maximum value of returned allotments is 1000
 *
 * strategy String Type of the search strategy. Could be range or update.<br>In a case of range strategy records will be searched by their effectiveDate / expireDate instead of searching by updated field in a case of update strategy
 * searchRequest SearchRequest 
 * returns inline_response_200
 **/
exports.searchAllotments = function(strategy,searchRequest) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "RecordSet" : {
    "total" : 0,
    "offset" : 6,
    "entities" : [ "{}", "{}" ]
  }
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

