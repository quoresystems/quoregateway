'use strict';


/**
 * Get rate by rateCode
 *
 * propertyCode String Property code as designated in the PMS
 * rateCode String Rate code
 * returns Rate
 **/
exports.getRate = function(propertyCode,rateCode) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "sourceCode" : "SRC",
  "repositoryUpdated" : "2018-06-01T15:34:54.000",
  "repositoryCreated" : "2018-06-01T15:34:54.000",
  "yieldStatus" : "FULLY",
  "marketCode" : "DM",
  "details" : [ {
    "sourceCode" : "SRC",
    "roomTypes" : "K1,Q1",
    "rates" : [ {
      "amount" : 59.5,
      "guests" : 1,
      "guestType" : "Adult"
    }, {
      "amount" : 59.5,
      "guests" : 1,
      "guestType" : "Adult"
    } ],
    "marketCode" : "DM",
    "dates" : {
      "start" : "2018-12-01",
      "end" : "2018-12-03"
    },
    "packages" : "ACTIVE",
    "daysOfWeek" : "FRIDAY,SATURDAY,SUNDAY",
    "rateCode" : "RACK"
  }, {
    "sourceCode" : "SRC",
    "roomTypes" : "K1,Q1",
    "rates" : [ {
      "amount" : 59.5,
      "guests" : 1,
      "guestType" : "Adult"
    }, {
      "amount" : 59.5,
      "guests" : 1,
      "guestType" : "Adult"
    } ],
    "marketCode" : "DM",
    "dates" : {
      "start" : "2018-12-01",
      "end" : "2018-12-03"
    },
    "packages" : "ACTIVE",
    "daysOfWeek" : "FRIDAY,SATURDAY,SUNDAY",
    "rateCode" : "RACK"
  } ],
  "id" : "BAR",
  "category" : "STD",
  "propertyId" : {
    "propertyCode" : "FSDH",
    "chainCode" : "HIL",
    "brandCode" : "ABVI"
  },
  "sellDates" : {
    "start" : "2018-12-01",
    "end" : "2018-12-03"
  },
  "status" : "ACTIVE"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Get rates by date range. Paging is included. Maximum value of returned rates is 1000
 *
 * searchRequest SearchRequest 
 * returns inline_response_200
 **/
exports.getRates = function(searchRequest) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "RecordSet" : {
    "total" : 0,
    "offset" : 6,
    "entities" : [ "{}", "{}" ]
  }
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

