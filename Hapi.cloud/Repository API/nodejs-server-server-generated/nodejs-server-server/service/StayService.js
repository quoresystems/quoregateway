'use strict';


/**
 * Get stays by reservation number
 *
 * propertyCode String Property code as designated in the PMS
 * reservationId String PMS Reservation id
 * returns Stay
 **/
exports.getStays = function(propertyCode,reservationId) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "repositoryUpdated" : "2018-06-01T15:34:54.000",
  "reservationId" : "542154",
  "repositoryCreated" : "2018-06-01T15:34:54.000",
  "propertyId" : {
    "propertyCode" : "FSDH",
    "chainCode" : "HIL",
    "brandCode" : "ABVI"
  },
  "roomStay" : {
    "roomCount" : 1,
    "blockCode" : "GROUPBLOCK",
    "roomNumber" : "122",
    "rates" : [ {
      "amount" : 105.55,
      "currencyCode" : "USD",
      "effectiveDates" : {
        "start" : "2018-12-01",
        "end" : "2018-12-03"
      },
      "rateCode" : "AAA"
    }, {
      "amount" : 105.55,
      "currencyCode" : "USD",
      "effectiveDates" : {
        "start" : "2018-12-01",
        "end" : "2018-12-03"
      },
      "rateCode" : "AAA"
    } ],
    "services" : [ {
      "rate" : {
        "amount" : 105.55,
        "currencyCode" : "USD",
        "effectiveDates" : {
          "start" : "2018-12-01",
          "end" : "2018-12-03"
        },
        "rateCode" : "AAA"
      },
      "dates" : {
        "start" : "2018-12-01",
        "end" : "2018-12-03"
      },
      "id" : "id"
    }, {
      "rate" : {
        "amount" : 105.55,
        "currencyCode" : "USD",
        "effectiveDates" : {
          "start" : "2018-12-01",
          "end" : "2018-12-03"
        },
        "rateCode" : "AAA"
      },
      "dates" : {
        "start" : "2018-12-01",
        "end" : "2018-12-03"
      },
      "id" : "id"
    } ],
    "rateCode" : "AAA",
    "sourceCode" : "SRC",
    "revenue" : [ {
      "date" : "2018-08-23",
      "amount" : 105.55,
      "type" : "Room",
      "currencyCode" : "USD"
    }, {
      "date" : "2018-08-23",
      "amount" : 105.55,
      "type" : "Room",
      "currencyCode" : "USD"
    } ],
    "marketCode" : "F",
    "paymentMethod" : "CA",
    "compYN" : "false",
    "guestId" : "5658451",
    "roomType" : "KING",
    "status" : "RESERVED",
    "channelCode" : "DM"
  }
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Get stays by date range. Paging is included. Maximum value of returned stays is 1000
 *
 * searchRequest SearchRequest 
 * returns inline_response_200
 **/
exports.searchStays = function(searchRequest) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "RecordSet" : {
    "total" : 0,
    "offset" : 6,
    "entities" : [ "{}", "{}" ]
  }
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

