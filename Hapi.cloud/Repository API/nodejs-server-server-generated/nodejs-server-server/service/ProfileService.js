'use strict';


/**
 * Get profile by id
 *
 * propertyCode String Property code as designated in the PMS
 * id String PMS profile id
 * returns Profile
 **/
exports.getProfile = function(propertyCode,id) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "addresses" : [ {
    "country" : {
      "code" : "US",
      "name" : "United States"
    },
    "address3" : "address3",
    "address2" : "APT 505",
    "city" : "Miami",
    "address1" : "1234 Park Ave",
    "postalCode" : "33133",
    "stateProvince" : {
      "code" : "FL",
      "name" : "Florida"
    },
    "type" : "BUSINESS",
    "primary" : true
  }, {
    "country" : {
      "code" : "US",
      "name" : "United States"
    },
    "address3" : "address3",
    "address2" : "APT 505",
    "city" : "Miami",
    "address1" : "1234 Park Ave",
    "postalCode" : "33133",
    "stateProvince" : {
      "code" : "FL",
      "name" : "Florida"
    },
    "type" : "BUSINESS",
    "primary" : true
  } ],
  "creator" : "AFESSE",
  "repositoryUpdated" : "2018-06-01T15:34:54.000",
  "repositoryCreated" : "2018-06-01T15:34:54.000",
  "phones" : [ {
    "number" : "555-555-1212",
    "type" : "MOBILE"
  }, {
    "number" : "555-555-1212",
    "type" : "MOBILE"
  } ],
  "dateOfBirth" : "1979-10-31",
  "type" : "PERSON",
  "emails" : [ {
    "address" : "cguest@email.com",
    "type" : "PRIVATE"
  }, {
    "address" : "cguest@email.com",
    "type" : "PRIVATE"
  } ],
  "travelAgentId" : "ABC1234",
  "idDocuments" : [ {
    "number" : "12345",
    "type" : "PASSPORT"
  }, {
    "number" : "12345",
    "type" : "PASSPORT"
  } ],
  "createdDate" : "2018-05-14T20:02:40.000",
  "loyaltyPrograms" : [ {
    "membershipId" : "HR6585421",
    "type" : "Hotel Rewards"
  }, {
    "membershipId" : "HR6585421",
    "type" : "Hotel Rewards"
  } ],
  "name" : {
    "middle" : "M",
    "last" : "Guest",
    "salutation" : "Mr.",
    "first" : "Christopher"
  },
  "company" : "HAPI",
  "id" : "542154",
  "propertyId" : {
    "propertyCode" : "FSDH",
    "chainCode" : "HIL",
    "brandCode" : "ABVI"
  }
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Get profiles by date range. Paging is included. Maximum value of returned profiles is 1000
 *
 * searchRequest SearchRequest 
 * returns inline_response_200
 **/
exports.getProfiles = function(searchRequest) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "RecordSet" : {
    "total" : 0,
    "offset" : 6,
    "entities" : [ "{}", "{}" ]
  }
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

