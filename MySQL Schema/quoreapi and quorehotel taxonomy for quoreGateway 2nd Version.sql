--
-- Script was generated by Devart dbForge Studio 2019 for MySQL, Version 8.1.22.0
-- Product home page: http://www.devart.com/dbforge/mysql/studio
-- Script date 4/12/2019 9:34:36 AM
-- Server version: 5.7.12
-- Client version: 4.1
--

SET NAMES 'utf8';

INSERT INTO quorehotel.area_status_cleaning(id, name, active) VALUES
('CLN', 'Clean', 1)
ON DUPLICATE KEY UPDATE id=VALUES(id), name=VALUES(name), active=VALUES(active);
INSERT INTO quorehotel.area_status_cleaning(id, name, active) VALUES
('DRT', 'Dirty', 1)
ON DUPLICATE KEY UPDATE id=VALUES(id), name=VALUES(name), active=VALUES(active);
INSERT INTO quorehotel.area_status_cleaning(id, name, active) VALUES
('INS', 'Inspected', 1)
ON DUPLICATE KEY UPDATE id=VALUES(id), name=VALUES(name), active=VALUES(active);
INSERT INTO quorehotel.area_status_cleaning(id, name, active) VALUES
('PIC', 'Pickup', 1)
ON DUPLICATE KEY UPDATE id=VALUES(id), name=VALUES(name), active=VALUES(active);
INSERT INTO quorehotel.area_status_cleaning(id, name, active) VALUES
('UNK', 'Unknown', 1)
ON DUPLICATE KEY UPDATE id=VALUES(id), name=VALUES(name), active=VALUES(active);

INSERT INTO quorehotel.area_status_service(id, name, active) VALUES
('OK', 'In Service', 1)
ON DUPLICATE KEY UPDATE id=VALUES(id), name=VALUES(name), active=VALUES(active);
INSERT INTO quorehotel.area_status_service(id, name, active) VALUES
('OOI', 'Out Of Inventory', 1)
ON DUPLICATE KEY UPDATE id=VALUES(id), name=VALUES(name), active=VALUES(active);
INSERT INTO quorehotel.area_status_service(id, name, active) VALUES
('OOO', 'Out Of Order', 1)
ON DUPLICATE KEY UPDATE id=VALUES(id), name=VALUES(name), active=VALUES(active);
INSERT INTO quorehotel.area_status_service(id, name, active) VALUES
('OOS', 'Out Of Service', 1)
ON DUPLICATE KEY UPDATE id=VALUES(id), name=VALUES(name), active=VALUES(active);

INSERT INTO quorehotel.area_status_stay(id, name, is_occupied, active) VALUES
('IN', 'Checked In', 1, 1)
ON DUPLICATE KEY UPDATE id=VALUES(id), name=VALUES(name), is_occupied=VALUES(is_occupied), active=VALUES(active);
INSERT INTO quorehotel.area_status_stay(id, name, is_occupied, active) VALUES
('INH', 'In House', 0, 1)
ON DUPLICATE KEY UPDATE id=VALUES(id), name=VALUES(name), is_occupied=VALUES(is_occupied), active=VALUES(active);
INSERT INTO quorehotel.area_status_stay(id, name, is_occupied, active) VALUES
('LST', 'Waiting List', 0, 1)
ON DUPLICATE KEY UPDATE id=VALUES(id), name=VALUES(name), is_occupied=VALUES(is_occupied), active=VALUES(active);
INSERT INTO quorehotel.area_status_stay(id, name, is_occupied, active) VALUES
('NSH', 'No Show', 0, 1)
ON DUPLICATE KEY UPDATE id=VALUES(id), name=VALUES(name), is_occupied=VALUES(is_occupied), active=VALUES(active);
INSERT INTO quorehotel.area_status_stay(id, name, is_occupied, active) VALUES
('OUT', 'Checked Out', 0, 1)
ON DUPLICATE KEY UPDATE id=VALUES(id), name=VALUES(name), is_occupied=VALUES(is_occupied), active=VALUES(active);
INSERT INTO quorehotel.area_status_stay(id, name, is_occupied, active) VALUES
('PRE', 'Pre Check-In', 0, 1)
ON DUPLICATE KEY UPDATE id=VALUES(id), name=VALUES(name), is_occupied=VALUES(is_occupied), active=VALUES(active);
INSERT INTO quorehotel.area_status_stay(id, name, is_occupied, active) VALUES
('REQ', 'Requested', 0, 1)
ON DUPLICATE KEY UPDATE id=VALUES(id), name=VALUES(name), is_occupied=VALUES(is_occupied), active=VALUES(active);
INSERT INTO quorehotel.area_status_stay(id, name, is_occupied, active) VALUES
('RES', 'Reserved', 0, 1)
ON DUPLICATE KEY UPDATE id=VALUES(id), name=VALUES(name), is_occupied=VALUES(is_occupied), active=VALUES(active);
INSERT INTO quorehotel.area_status_stay(id, name, is_occupied, active) VALUES
('TRF', 'Transfered', 0, 1)
ON DUPLICATE KEY UPDATE id=VALUES(id), name=VALUES(name), is_occupied=VALUES(is_occupied), active=VALUES(active);
INSERT INTO quorehotel.area_status_stay(id, name, is_occupied, active) VALUES
('UNK', 'Unknown', 0, 1)
ON DUPLICATE KEY UPDATE id=VALUES(id), name=VALUES(name), is_occupied=VALUES(is_occupied), active=VALUES(active);
INSERT INTO quorehotel.area_status_stay(id, name, is_occupied, active) VALUES
('X', 'Cancelled', 0, 1)
ON DUPLICATE KEY UPDATE id=VALUES(id), name=VALUES(name), is_occupied=VALUES(is_occupied), active=VALUES(active);

--
-- Script was generated by Devart dbForge Studio 2019 for MySQL, Version 8.1.22.0
-- Product home page: http://www.devart.com/dbforge/mysql/studio
-- Script date 4/12/2019 9:36:28 AM
-- Server version: 5.7.12
-- Client version: 4.1
--

INSERT INTO quoreapi.api_signers(id, signer_name, active, remarks, allow_use_of_all_areas, guest_name_to_be_displayed) VALUES
(200, 'StayNTouch', True, 'https://www.stayntouch.com/', True, 'Stay N Touch (PMS)')
ON DUPLICATE KEY UPDATE id=VALUES(id), signer_name=VALUES(signer_name), active=VALUES(active), remarks=VALUES(remarks), allow_use_of_all_areas=VALUES(allow_use_of_all_areas), guest_name_to_be_displayed=VALUES(guest_name_to_be_displayed);
INSERT INTO quoreapi.api_signers(id, signer_name, active, remarks, allow_use_of_all_areas, guest_name_to_be_displayed) VALUES
(201, 'Hapi', True, 'https://hapicloud.io/', True, 'Hapi Cloud (integrator)')
ON DUPLICATE KEY UPDATE id=VALUES(id), signer_name=VALUES(signer_name), active=VALUES(active), remarks=VALUES(remarks), allow_use_of_all_areas=VALUES(allow_use_of_all_areas), guest_name_to_be_displayed=VALUES(guest_name_to_be_displayed);
INSERT INTO quoreapi.api_signers(id, signer_name, active, remarks, allow_use_of_all_areas, guest_name_to_be_displayed) VALUES
(202, 'Impala', True, 'https://getimpala.com/', True, 'Impala (integrator)')
ON DUPLICATE KEY UPDATE id=VALUES(id), signer_name=VALUES(signer_name), active=VALUES(active), remarks=VALUES(remarks), allow_use_of_all_areas=VALUES(allow_use_of_all_areas), guest_name_to_be_displayed=VALUES(guest_name_to_be_displayed);
INSERT INTO quoreapi.api_signers(id, signer_name, active, remarks, allow_use_of_all_areas, guest_name_to_be_displayed) VALUES
(203, 'SiteMind', True, 'https://www.siteminder.com/', True, 'SiteMinder (integrator)')
ON DUPLICATE KEY UPDATE id=VALUES(id), signer_name=VALUES(signer_name), active=VALUES(active), remarks=VALUES(remarks), allow_use_of_all_areas=VALUES(allow_use_of_all_areas), guest_name_to_be_displayed=VALUES(guest_name_to_be_displayed);

INSERT INTO quoreapi.api_signers_endpoint(id, api_signers_id, started_on, ended_on, entpoint_url, action, action_regex, version, environment, system_name, direction, params) VALUES
(1, 200, '2019-03-06 08:39:41', NULL, 'localhost', 'InitializeStayNTouchHotel', '', '0.1', NULL, NULL, 'ToQuore', NULL)
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), started_on=VALUES(started_on), ended_on=VALUES(ended_on), entpoint_url=VALUES(entpoint_url), action=VALUES(action), action_regex=VALUES(action_regex), version=VALUES(version), environment=VALUES(environment), system_name=VALUES(system_name), direction=VALUES(direction), params=VALUES(params);
INSERT INTO quoreapi.api_signers_endpoint(id, api_signers_id, started_on, ended_on, entpoint_url, action, action_regex, version, environment, system_name, direction, params) VALUES
(2, 200, '2019-04-09 17:26:16', NULL, 'https://auth-dev.stayntouch.com/oauth/token', 'Oauth2.0', NULL, '1', 'DEV', 'Authorization', 'FromQuore', NULL)
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), started_on=VALUES(started_on), ended_on=VALUES(ended_on), entpoint_url=VALUES(entpoint_url), action=VALUES(action), action_regex=VALUES(action_regex), version=VALUES(version), environment=VALUES(environment), system_name=VALUES(system_name), direction=VALUES(direction), params=VALUES(params);
INSERT INTO quoreapi.api_signers_endpoint(id, api_signers_id, started_on, ended_on, entpoint_url, action, action_regex, version, environment, system_name, direction, params) VALUES
(3, 200, '2019-04-10 19:20:08', NULL, 'https://pms-dev.stayntouch.com/connect/hotels/:hotelId/rooms?page=1&per_page=1000', 'getRoomsStatus', NULL, '1', 'DEV', NULL, 'FromQuore', NULL)
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), started_on=VALUES(started_on), ended_on=VALUES(ended_on), entpoint_url=VALUES(entpoint_url), action=VALUES(action), action_regex=VALUES(action_regex), version=VALUES(version), environment=VALUES(environment), system_name=VALUES(system_name), direction=VALUES(direction), params=VALUES(params);

INSERT INTO quoreapi.api_signers_map_area_status_cleaning(id, api_signers_id, signer_area_status_cleaning_code, quorehotel_area_status_cleaning_id, quorehotel_area_status_service_id) VALUES
(1, 200, 'CLEAN', 'CLN', NULL)
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_cleaning_code=VALUES(signer_area_status_cleaning_code), quorehotel_area_status_cleaning_id=VALUES(quorehotel_area_status_cleaning_id), quorehotel_area_status_service_id=VALUES(quorehotel_area_status_service_id);
INSERT INTO quoreapi.api_signers_map_area_status_cleaning(id, api_signers_id, signer_area_status_cleaning_code, quorehotel_area_status_cleaning_id, quorehotel_area_status_service_id) VALUES
(2, 200, 'DIRTY', 'DRT', NULL)
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_cleaning_code=VALUES(signer_area_status_cleaning_code), quorehotel_area_status_cleaning_id=VALUES(quorehotel_area_status_cleaning_id), quorehotel_area_status_service_id=VALUES(quorehotel_area_status_service_id);
INSERT INTO quoreapi.api_signers_map_area_status_cleaning(id, api_signers_id, signer_area_status_cleaning_code, quorehotel_area_status_cleaning_id, quorehotel_area_status_service_id) VALUES
(3, 200, 'INSPECTED', 'INS', NULL)
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_cleaning_code=VALUES(signer_area_status_cleaning_code), quorehotel_area_status_cleaning_id=VALUES(quorehotel_area_status_cleaning_id), quorehotel_area_status_service_id=VALUES(quorehotel_area_status_service_id);
INSERT INTO quoreapi.api_signers_map_area_status_cleaning(id, api_signers_id, signer_area_status_cleaning_code, quorehotel_area_status_cleaning_id, quorehotel_area_status_service_id) VALUES
(4, 200, 'PICKUP', 'PIC', NULL)
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_cleaning_code=VALUES(signer_area_status_cleaning_code), quorehotel_area_status_cleaning_id=VALUES(quorehotel_area_status_cleaning_id), quorehotel_area_status_service_id=VALUES(quorehotel_area_status_service_id);
INSERT INTO quoreapi.api_signers_map_area_status_cleaning(id, api_signers_id, signer_area_status_cleaning_code, quorehotel_area_status_cleaning_id, quorehotel_area_status_service_id) VALUES
(5, 202, 'CLEAN', 'CLN', NULL)
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_cleaning_code=VALUES(signer_area_status_cleaning_code), quorehotel_area_status_cleaning_id=VALUES(quorehotel_area_status_cleaning_id), quorehotel_area_status_service_id=VALUES(quorehotel_area_status_service_id);
INSERT INTO quoreapi.api_signers_map_area_status_cleaning(id, api_signers_id, signer_area_status_cleaning_code, quorehotel_area_status_cleaning_id, quorehotel_area_status_service_id) VALUES
(6, 202, 'DIRTY', 'DRT', NULL)
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_cleaning_code=VALUES(signer_area_status_cleaning_code), quorehotel_area_status_cleaning_id=VALUES(quorehotel_area_status_cleaning_id), quorehotel_area_status_service_id=VALUES(quorehotel_area_status_service_id);
INSERT INTO quoreapi.api_signers_map_area_status_cleaning(id, api_signers_id, signer_area_status_cleaning_code, quorehotel_area_status_cleaning_id, quorehotel_area_status_service_id) VALUES
(7, 202, 'INSPECTED', 'INS', NULL)
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_cleaning_code=VALUES(signer_area_status_cleaning_code), quorehotel_area_status_cleaning_id=VALUES(quorehotel_area_status_cleaning_id), quorehotel_area_status_service_id=VALUES(quorehotel_area_status_service_id);
INSERT INTO quoreapi.api_signers_map_area_status_cleaning(id, api_signers_id, signer_area_status_cleaning_code, quorehotel_area_status_cleaning_id, quorehotel_area_status_service_id) VALUES
(8, 202, 'INDETERMINATE', 'UNK', NULL)
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_cleaning_code=VALUES(signer_area_status_cleaning_code), quorehotel_area_status_cleaning_id=VALUES(quorehotel_area_status_cleaning_id), quorehotel_area_status_service_id=VALUES(quorehotel_area_status_service_id);
INSERT INTO quoreapi.api_signers_map_area_status_cleaning(id, api_signers_id, signer_area_status_cleaning_code, quorehotel_area_status_cleaning_id, quorehotel_area_status_service_id) VALUES
(9, 202, 'OTHER', 'UNK', NULL)
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_cleaning_code=VALUES(signer_area_status_cleaning_code), quorehotel_area_status_cleaning_id=VALUES(quorehotel_area_status_cleaning_id), quorehotel_area_status_service_id=VALUES(quorehotel_area_status_service_id);
INSERT INTO quoreapi.api_signers_map_area_status_cleaning(id, api_signers_id, signer_area_status_cleaning_code, quorehotel_area_status_cleaning_id, quorehotel_area_status_service_id) VALUES
(10, 202, 'OUT_OF_SERVICE', 'DRT', 'OOS')
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_cleaning_code=VALUES(signer_area_status_cleaning_code), quorehotel_area_status_cleaning_id=VALUES(quorehotel_area_status_cleaning_id), quorehotel_area_status_service_id=VALUES(quorehotel_area_status_service_id);
INSERT INTO quoreapi.api_signers_map_area_status_cleaning(id, api_signers_id, signer_area_status_cleaning_code, quorehotel_area_status_cleaning_id, quorehotel_area_status_service_id) VALUES
(11, 202, 'OUT_OF_ORDER', 'DRT', 'OOO')
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_cleaning_code=VALUES(signer_area_status_cleaning_code), quorehotel_area_status_cleaning_id=VALUES(quorehotel_area_status_cleaning_id), quorehotel_area_status_service_id=VALUES(quorehotel_area_status_service_id);
INSERT INTO quoreapi.api_signers_map_area_status_cleaning(id, api_signers_id, signer_area_status_cleaning_code, quorehotel_area_status_cleaning_id, quorehotel_area_status_service_id) VALUES
(12, 202, 'OUT_OF_INVENTORY', 'DRT', 'OOI')
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_cleaning_code=VALUES(signer_area_status_cleaning_code), quorehotel_area_status_cleaning_id=VALUES(quorehotel_area_status_cleaning_id), quorehotel_area_status_service_id=VALUES(quorehotel_area_status_service_id);

INSERT INTO quoreapi.api_signers_map_area_status_service(api_signers_id, signer_area_status_service_code, quorehotel_area_status_service_id) VALUES
(200, 'IN_SERVICE', 'OK')
ON DUPLICATE KEY UPDATE api_signers_id=VALUES(api_signers_id), signer_area_status_service_code=VALUES(signer_area_status_service_code), quorehotel_area_status_service_id=VALUES(quorehotel_area_status_service_id);
INSERT INTO quoreapi.api_signers_map_area_status_service(api_signers_id, signer_area_status_service_code, quorehotel_area_status_service_id) VALUES
(200, 'OUT_OF_ORDER', 'OOO')
ON DUPLICATE KEY UPDATE api_signers_id=VALUES(api_signers_id), signer_area_status_service_code=VALUES(signer_area_status_service_code), quorehotel_area_status_service_id=VALUES(quorehotel_area_status_service_id);
INSERT INTO quoreapi.api_signers_map_area_status_service(api_signers_id, signer_area_status_service_code, quorehotel_area_status_service_id) VALUES
(200, 'OUT_OF_SERVICE', 'OOS')
ON DUPLICATE KEY UPDATE api_signers_id=VALUES(api_signers_id), signer_area_status_service_code=VALUES(signer_area_status_service_code), quorehotel_area_status_service_id=VALUES(quorehotel_area_status_service_id);

INSERT INTO quoreapi.api_signers_map_area_status_stay(id, api_signers_id, signer_area_status_stay_code, quorehotel_area_status_stay_id) VALUES
(1, 200, 'CHECKEDOUT', 'OUT')
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_stay_code=VALUES(signer_area_status_stay_code), quorehotel_area_status_stay_id=VALUES(quorehotel_area_status_stay_id);
INSERT INTO quoreapi.api_signers_map_area_status_stay(id, api_signers_id, signer_area_status_stay_code, quorehotel_area_status_stay_id) VALUES
(2, 200, 'RESERVED', 'RES')
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_stay_code=VALUES(signer_area_status_stay_code), quorehotel_area_status_stay_id=VALUES(quorehotel_area_status_stay_id);
INSERT INTO quoreapi.api_signers_map_area_status_stay(id, api_signers_id, signer_area_status_stay_code, quorehotel_area_status_stay_id) VALUES
(3, 200, 'CHECKEDIN', 'IN')
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_stay_code=VALUES(signer_area_status_stay_code), quorehotel_area_status_stay_id=VALUES(quorehotel_area_status_stay_id);
INSERT INTO quoreapi.api_signers_map_area_status_stay(id, api_signers_id, signer_area_status_stay_code, quorehotel_area_status_stay_id) VALUES
(4, 200, 'NOSHOW', 'NSH')
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_stay_code=VALUES(signer_area_status_stay_code), quorehotel_area_status_stay_id=VALUES(quorehotel_area_status_stay_id);
INSERT INTO quoreapi.api_signers_map_area_status_stay(id, api_signers_id, signer_area_status_stay_code, quorehotel_area_status_stay_id) VALUES
(5, 200, 'CANCELLED', 'X')
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_stay_code=VALUES(signer_area_status_stay_code), quorehotel_area_status_stay_id=VALUES(quorehotel_area_status_stay_id);
INSERT INTO quoreapi.api_signers_map_area_status_stay(id, api_signers_id, signer_area_status_stay_code, quorehotel_area_status_stay_id) VALUES
(6, 200, 'PRE_CHECKIN', 'PRE')
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_stay_code=VALUES(signer_area_status_stay_code), quorehotel_area_status_stay_id=VALUES(quorehotel_area_status_stay_id);
INSERT INTO quoreapi.api_signers_map_area_status_stay(id, api_signers_id, signer_area_status_stay_code, quorehotel_area_status_stay_id) VALUES
(7, 201, 'UNKNOWN', 'UNK')
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_stay_code=VALUES(signer_area_status_stay_code), quorehotel_area_status_stay_id=VALUES(quorehotel_area_status_stay_id);
INSERT INTO quoreapi.api_signers_map_area_status_stay(id, api_signers_id, signer_area_status_stay_code, quorehotel_area_status_stay_id) VALUES
(8, 201, 'REQUESTED', 'REQ')
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_stay_code=VALUES(signer_area_status_stay_code), quorehotel_area_status_stay_id=VALUES(quorehotel_area_status_stay_id);
INSERT INTO quoreapi.api_signers_map_area_status_stay(id, api_signers_id, signer_area_status_stay_code, quorehotel_area_status_stay_id) VALUES
(9, 201, 'RESERVED', 'RES')
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_stay_code=VALUES(signer_area_status_stay_code), quorehotel_area_status_stay_id=VALUES(quorehotel_area_status_stay_id);
INSERT INTO quoreapi.api_signers_map_area_status_stay(id, api_signers_id, signer_area_status_stay_code, quorehotel_area_status_stay_id) VALUES
(10, 201, 'CANCELLED', 'X')
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_stay_code=VALUES(signer_area_status_stay_code), quorehotel_area_status_stay_id=VALUES(quorehotel_area_status_stay_id);
INSERT INTO quoreapi.api_signers_map_area_status_stay(id, api_signers_id, signer_area_status_stay_code, quorehotel_area_status_stay_id) VALUES
(11, 201, 'IN_HOUSE', 'INH')
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_stay_code=VALUES(signer_area_status_stay_code), quorehotel_area_status_stay_id=VALUES(quorehotel_area_status_stay_id);
INSERT INTO quoreapi.api_signers_map_area_status_stay(id, api_signers_id, signer_area_status_stay_code, quorehotel_area_status_stay_id) VALUES
(12, 201, 'CHECKED_OUT', 'OUT')
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_stay_code=VALUES(signer_area_status_stay_code), quorehotel_area_status_stay_id=VALUES(quorehotel_area_status_stay_id);
INSERT INTO quoreapi.api_signers_map_area_status_stay(id, api_signers_id, signer_area_status_stay_code, quorehotel_area_status_stay_id) VALUES
(13, 201, 'NO_SHOW', 'NSH')
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_stay_code=VALUES(signer_area_status_stay_code), quorehotel_area_status_stay_id=VALUES(quorehotel_area_status_stay_id);
INSERT INTO quoreapi.api_signers_map_area_status_stay(id, api_signers_id, signer_area_status_stay_code, quorehotel_area_status_stay_id) VALUES
(14, 201, 'WAIT_LIST', 'LST')
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_stay_code=VALUES(signer_area_status_stay_code), quorehotel_area_status_stay_id=VALUES(quorehotel_area_status_stay_id);
INSERT INTO quoreapi.api_signers_map_area_status_stay(id, api_signers_id, signer_area_status_stay_code, quorehotel_area_status_stay_id) VALUES
(15, 202, 'OTHER', 'UNK')
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_stay_code=VALUES(signer_area_status_stay_code), quorehotel_area_status_stay_id=VALUES(quorehotel_area_status_stay_id);
INSERT INTO quoreapi.api_signers_map_area_status_stay(id, api_signers_id, signer_area_status_stay_code, quorehotel_area_status_stay_id) VALUES
(16, 202, 'REQUESTED', 'REQ')
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_stay_code=VALUES(signer_area_status_stay_code), quorehotel_area_status_stay_id=VALUES(quorehotel_area_status_stay_id);
INSERT INTO quoreapi.api_signers_map_area_status_stay(id, api_signers_id, signer_area_status_stay_code, quorehotel_area_status_stay_id) VALUES
(17, 202, 'EXPECTED', 'PRE')
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_stay_code=VALUES(signer_area_status_stay_code), quorehotel_area_status_stay_id=VALUES(quorehotel_area_status_stay_id);
INSERT INTO quoreapi.api_signers_map_area_status_stay(id, api_signers_id, signer_area_status_stay_code, quorehotel_area_status_stay_id) VALUES
(18, 202, 'CANCELLED', 'X')
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_stay_code=VALUES(signer_area_status_stay_code), quorehotel_area_status_stay_id=VALUES(quorehotel_area_status_stay_id);
INSERT INTO quoreapi.api_signers_map_area_status_stay(id, api_signers_id, signer_area_status_stay_code, quorehotel_area_status_stay_id) VALUES
(19, 202, 'CHECKED_IN', 'IN')
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_stay_code=VALUES(signer_area_status_stay_code), quorehotel_area_status_stay_id=VALUES(quorehotel_area_status_stay_id);
INSERT INTO quoreapi.api_signers_map_area_status_stay(id, api_signers_id, signer_area_status_stay_code, quorehotel_area_status_stay_id) VALUES
(20, 202, 'CHECKED_OUT', 'OUT')
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_stay_code=VALUES(signer_area_status_stay_code), quorehotel_area_status_stay_id=VALUES(quorehotel_area_status_stay_id);
INSERT INTO quoreapi.api_signers_map_area_status_stay(id, api_signers_id, signer_area_status_stay_code, quorehotel_area_status_stay_id) VALUES
(21, 202, 'NO_SHOW', 'NSH')
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_stay_code=VALUES(signer_area_status_stay_code), quorehotel_area_status_stay_id=VALUES(quorehotel_area_status_stay_id);
INSERT INTO quoreapi.api_signers_map_area_status_stay(id, api_signers_id, signer_area_status_stay_code, quorehotel_area_status_stay_id) VALUES
(22, 202, 'TRANSFERED', 'TRF')
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_stay_code=VALUES(signer_area_status_stay_code), quorehotel_area_status_stay_id=VALUES(quorehotel_area_status_stay_id);

