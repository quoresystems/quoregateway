﻿
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

SET NAMES 'utf8';

CREATE TABLE IF NOT EXISTS api_signers_map_property (
  id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  api_signers_id INT(11) NOT NULL,
  quorehotel_property_id INT(11) DEFAULT NULL,
  signer_hotel_id VARCHAR(255) DEFAULT NULL,
  signer_other_id VARCHAR(255) DEFAULT NULL,
  signer_hotel_code VARCHAR(25) DEFAULT NULL,
  signer_hotel_name VARCHAR(255) DEFAULT NULL,
  signer_hotel_chain VARCHAR(255) DEFAULT NULL,
  signer_hotel_brand VARCHAR(255) DEFAULT NULL,
  inserted_on DATETIME DEFAULT CURRENT_TIMESTAMP,
  deactivated_on DATETIME DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB;

ALTER TABLE api_signers_map_property 
  ADD INDEX `fk.api_signers_map_property.api_signers_id`(api_signers_id);

ALTER TABLE api_signers_map_property 
  ADD INDEX `fk.api_signers_map_property.quorehotel_property_id`(quorehotel_property_id);

ALTER TABLE api_signers_map_property 
  ADD INDEX `idx.api_signers_map_property.signer_hotel_chain`(signer_hotel_chain);

ALTER TABLE api_signers_map_property 
  ADD INDEX `idx.api_signers_map_property.signer_hotel_code`(signer_hotel_code);

ALTER TABLE api_signers_map_property 
  ADD INDEX `idx.api_signers_map_property.signer_hotel_id`(signer_hotel_id);

ALTER TABLE api_signers_map_property 
  ADD INDEX `idx.api_signers_map_property.signer_other_id`(signer_other_id);

ALTER TABLE api_signers_map_property 
  ADD CONSTRAINT `fk.api_signers_map_property.api_signers` FOREIGN KEY (api_signers_id)
    REFERENCES api_signers(id) ON DELETE NO ACTION;

ALTER TABLE api_signers_map_property 
  ADD CONSTRAINT `fk.api_signers_map_property.quorehotel.property` FOREIGN KEY (quorehotel_property_id)
    REFERENCES quorehotel.property(id) ON UPDATE CASCADE;

CREATE TABLE IF NOT EXISTS api_signers_map_property_area (
  api_signers_map_property_id INT(11) UNSIGNED NOT NULL,
  signer_room_id VARCHAR(50) NOT NULL,
  signer_room_name VARCHAR(50) DEFAULT NULL,
  quorehotel_area_base_id INT(11) UNSIGNED DEFAULT NULL,
  signer_room_type_name VARCHAR(50) DEFAULT NULL,
  PRIMARY KEY (api_signers_map_property_id)
)
ENGINE = INNODB;

ALTER TABLE api_signers_map_property_area 
  ADD INDEX `idx.api_signers_map_property_area.quorehotel.area_base`(quorehotel_area_base_id);

ALTER TABLE api_signers_map_property_area 
  ADD INDEX `idx.api_signers_map_property_area.roomNumber`(signer_room_name);

ALTER TABLE api_signers_map_property_area 
  ADD INDEX `idx.api_signers_map_property_area.signer_room_type_name`(signer_room_type_name);

ALTER TABLE api_signers_map_property_area 
  ADD CONSTRAINT FK_api_signers_map_property_area_quorehotel_area_base_id FOREIGN KEY (quorehotel_area_base_id)
    REFERENCES quorehotel.area_base(id) ON UPDATE CASCADE;

ALTER TABLE api_signers_map_property_area 
  ADD CONSTRAINT `fk.api_signers_map_property_area.api_signers_map_property` FOREIGN KEY (api_signers_map_property_id)
    REFERENCES api_signers_map_property(id) ON DELETE CASCADE;

CREATE TABLE IF NOT EXISTS api_signers_entity (
  id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  api_signers_id INT(11) DEFAULT NULL,
  signer_entity_id VARCHAR(255) DEFAULT NULL,
  type ENUM('Company','TravelAgency','Other') DEFAULT NULL,
  api_signers_map_property_id INT(11) UNSIGNED DEFAULT NULL,
  address_line_1 VARCHAR(255) DEFAULT NULL,
  address_line_2 VARCHAR(255) DEFAULT NULL,
  address_line_3 VARCHAR(255) DEFAULT NULL,
  city VARCHAR(255) DEFAULT NULL,
  state VARCHAR(50) DEFAULT NULL,
  postal_code VARCHAR(50) DEFAULT NULL,
  iso_country_code VARCHAR(10) DEFAULT NULL,
  inserted_on DATETIME DEFAULT CURRENT_TIMESTAMP,
  updated_on DATETIME DEFAULT NULL,
  deactivated_on DATETIME DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB;

ALTER TABLE api_signers_entity 
  ADD INDEX `idx.api_signers_entity_signer_entity_id`(signer_entity_id);

ALTER TABLE api_signers_entity 
  ADD INDEX `idx.api_signers_entity_type`(type);

ALTER TABLE api_signers_entity 
  ADD CONSTRAINT FK_api_signers_entity_api_signers_id FOREIGN KEY (api_signers_id)
    REFERENCES api_signers(id) ON UPDATE CASCADE;

ALTER TABLE api_signers_entity 
  ADD CONSTRAINT `fk.api_signers_entity.api_signers_map_property` FOREIGN KEY (api_signers_map_property_id)
    REFERENCES api_signers_map_property(id) ON UPDATE CASCADE;

CREATE TABLE IF NOT EXISTS api_signers_map_area_status_stay (
  id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  api_signers_id INT(11) NOT NULL,
  signer_area_status_stay_code VARCHAR(50) NOT NULL,
  quorehotel_area_status_stay_id CHAR(3) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB;

ALTER TABLE api_signers_map_area_status_stay 
  ADD INDEX `idx.api_signers_map_area_status_stay.signer_area_status_stay_cod`(signer_area_status_stay_code);

ALTER TABLE api_signers_map_area_status_stay 
  ADD CONSTRAINT `fk.api_signers_map_area_status_stay.api_signers` FOREIGN KEY (api_signers_id)
    REFERENCES api_signers(id) ON DELETE NO ACTION;

ALTER TABLE api_signers_map_area_status_stay 
  ADD CONSTRAINT `fk.api_signers_map_area_status_stay.quorehotel.area_status_stay` FOREIGN KEY (quorehotel_area_status_stay_id)
    REFERENCES quorehotel.area_status_stay(id) ON DELETE NO ACTION;

CREATE TABLE IF NOT EXISTS api_signers_map_area_status_service (
  api_signers_id INT(11) NOT NULL,
  signer_area_status_service_code VARCHAR(50) NOT NULL,
  quorehotel_area_status_service_id CHAR(3) NOT NULL,
  PRIMARY KEY (api_signers_id, signer_area_status_service_code, quorehotel_area_status_service_id)
)
ENGINE = INNODB;

ALTER TABLE api_signers_map_area_status_service 
  ADD INDEX `idx.api_signers_map_area_status_service.signer_area_status_servi`(signer_area_status_service_code);

ALTER TABLE api_signers_map_area_status_service 
  ADD CONSTRAINT `fk.api_signers_area_status_service.quorehotel.area_status_serv` FOREIGN KEY (quorehotel_area_status_service_id)
    REFERENCES quorehotel.area_status_service(id) ON UPDATE CASCADE;

ALTER TABLE api_signers_map_area_status_service 
  ADD CONSTRAINT `fk.api_signers_map_area_service_status.api_signers` FOREIGN KEY (api_signers_id)
    REFERENCES api_signers(id) ON UPDATE CASCADE;

CREATE TABLE IF NOT EXISTS api_signers_map_area_status_cleaning (
  id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  api_signers_id INT(11) NOT NULL,
  signer_area_status_cleaning_code VARCHAR(50) NOT NULL,
  quorehotel_area_status_cleaning_id CHAR(3) NOT NULL,
  quorehotel_area_status_service_id CHAR(3) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB;

ALTER TABLE api_signers_map_area_status_cleaning 
  ADD INDEX `idx.api_signers_map_area_status_cleaning.signer_area_status_clea`(signer_area_status_cleaning_code);

ALTER TABLE api_signers_map_area_status_cleaning 
  ADD CONSTRAINT `fk.api_signers_map_area_status_cleaning.api_signers` FOREIGN KEY (api_signers_id)
    REFERENCES api_signers(id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE api_signers_map_area_status_cleaning 
  ADD CONSTRAINT `fk.api_signers_map_area_status_cleaning.quorehotel.a_s_c` FOREIGN KEY (quorehotel_area_status_cleaning_id)
    REFERENCES quorehotel.area_status_cleaning(id) ON UPDATE CASCADE;

ALTER TABLE api_signers_map_area_status_cleaning 
  ADD CONSTRAINT `fk.api_signers_map_area_status_cleaning.quorehotel.a_s_s` FOREIGN KEY (quorehotel_area_status_service_id)
    REFERENCES quorehotel.area_status_service(id) ON UPDATE CASCADE;

CREATE TABLE IF NOT EXISTS api_signers_endpoint (
  id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  api_signers_id INT(11) NOT NULL,
  started_on DATETIME DEFAULT CURRENT_TIMESTAMP,
  ended_on DATETIME DEFAULT NULL,
  entpoint_url TEXT DEFAULT NULL,
  action VARCHAR(50) DEFAULT NULL,
  action_regex VARCHAR(255) DEFAULT NULL,
  version CHAR(25) DEFAULT NULL,
  environment CHAR(100) DEFAULT NULL,
  system_name VARCHAR(50) DEFAULT NULL,
  direction ENUM('ToQuore','FromQuore') DEFAULT NULL,
  params JSON DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB;

ALTER TABLE api_signers_endpoint 
  ADD CONSTRAINT `fk.api_signers_endpoint.api_signers_id` FOREIGN KEY (api_signers_id)
    REFERENCES api_signers(id) ON DELETE CASCADE ON UPDATE CASCADE;

CREATE TABLE IF NOT EXISTS api_signers_endpoint_operation (
  id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  api_signers_endpoint_id INT(11) UNSIGNED NOT NULL,
  posted_on DATETIME DEFAULT CURRENT_TIMESTAMP,
  event VARCHAR(100) DEFAULT NULL,
  data JSON DEFAULT NULL,
  post_process_response JSON DEFAULT NULL,
  post_process_response_date DATETIME DEFAULT NULL,
  post_process_successful TINYINT(4) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (id)
)
ENGINE = INNODB;

ALTER TABLE api_signers_endpoint_operation 
  ADD INDEX `idx.api_signers_endpoint_operation.event.posted_on`(event, posted_on);

ALTER TABLE api_signers_endpoint_operation 
  ADD INDEX `idx.api_signers_endpoint_operation.success.event.date`(post_process_successful, event, posted_on);

ALTER TABLE api_signers_endpoint_operation 
  ADD CONSTRAINT `fk.api_signers_endpoint_operation.api_signers_endpoint` FOREIGN KEY (api_signers_endpoint_id)
    REFERENCES api_signers_endpoint(id) ON UPDATE CASCADE;

CREATE TABLE IF NOT EXISTS api_signers_map_user (
  id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  api_signers_id INT(11) DEFAULT NULL,
  signer_guest_id VARCHAR(255) DEFAULT NULL,
  user_id INT(11) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB;

ALTER TABLE api_signers_map_user 
  ADD INDEX `idx.api_signers_map_user.signer_guest_id`(signer_guest_id);

ALTER TABLE api_signers_map_user 
  ADD CONSTRAINT `fk.api_signers_map_user.api_signers` FOREIGN KEY (api_signers_id)
    REFERENCES api_signers(id) ON UPDATE CASCADE;

ALTER TABLE api_signers_map_user 
  ADD CONSTRAINT `fk.api_signers_map_user.user` FOREIGN KEY (user_id)
    REFERENCES user(id) ON UPDATE CASCADE;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;