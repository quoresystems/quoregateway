﻿CALL AddPMSEvent(1, 'InitializeStayNTouchHotel', CAST('{"id": 290, "code": "AVIDASS", "name": "Avida", "uuid": "d9842556-f88a-4a6d-9740-c6627339de48", "phone": "5713063394", "address": {"city": "Herndon", "state": "VA", "country": "SE", "address1": "230 TruxMore Ct", "address2": null, "address3": null, "postal_code": "20170"}, "main_contact": {"email": "garry@stayntouch.com", "phone": "5713063394", "last_name": "Tester", "first_name": "Garry"}, "check_in_time": "14:00", "currency_code": "EUR", "check_out_time": "11:00"}' AS JSON), 0);

CALL AddPMSEvent(1, 'xxx', '{"id": 290, "code": "AVIDA", "name": "Avida", "uuid": "d9842556-f88a-4a6d-9740-c6627339de48", "phone": "5713063394", "address": {"city": "Herndon", "state": "VA", "country": "SE", "address1": "230 TruxMore Ct", "address2": null, "address3": null, "postal_code": "20170"}, "main_contact": {"email": "garry@stayntouch.com", "phone": "5713063394", "last_name": "Tester", "first_name": "Garry"}, "check_in_time": "14:00", "currency_code": "EUR", "check_out_time": "11:00"}', 1);

SELECT ROUTINE_NAME INTO @x
    FROM information_schema.ROUTINES
    WHERE ROUTINE_TYPE = 'PROCEDURE'
        AND ROUTINE_SCHEMA = 'quoreapi'
        AND ROUTINE_NAME = 'InitializeStayNTouchHotel';

SELECT * FROM information_schema.ROUTINES;
UPDATE information_schema.ROUTINES SET ROUTINE_CATALOG = 'Nando' WHERE ROUTINE_NAME = 'InitializeStayNTouchHotel';

SHOW PROCEDURE STATUS;
SHOW VARIABLES;
CALL sys.ps_setup_show_enabled(TRUE, TRUE);
CALL statement_performance_analyzer();
CALL sys.diagnostics(120, 30, 'current');

SHOW WARNINGS;
SHOW ERRORS;
GET CURRENT DIAGNOSTICS CONDITION 1 @p1 = RETURNED_SQLSTATE;
SELECT * FROM quorehotel.user WHERE id = 0;
SELECT BENCHMARK(10, SELECT * FROM quorehotel.user WHERE id = 205);
SELECT @DiagNumber, @DiagRows;

GET CURRENT DIAGNOSTICS
    @DiagNumber = NUMBER,
    @DiagRows = ROW_COUNT;

SELECT * FROM user WHERE ID > 10 LIMIT 10 PROCEDURE 
SELECT FOUND_ROWS();

SELECT @@lc_time_names;

SELECT DAYNAME('2010-01-01'), MONTHNAME('2010-01-01');
    
SELECT FORMAT(12332.2,2,'pt_BR');

SET lc_time_names = 'en_US';

SET @x = '{"id": 290, "code": "AVIDASS", "name": "Avida", "uuid": "d9842556-f88a-4a6d-9740-c6627339de48", "phone": "5713063394", "address": {"city": "Herndon", "state": "VA", "country": "SE", "address1": "230 TruxMore Ct", "address2": null, "address3": null, "postal_code": "20170"}, "main_contact": {"email": "garry@stayntouch.com", "phone": "5713063394", "last_name": "Tester", "first_name": "Garry"}, "check_in_time": "14:00", "currency_code": "EUR", "check_out_time": "11:00"}';
SET @y = COMPRESS(@x);
SELECT LENGTH(@x), LENGTH(@y);
