﻿
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

SET NAMES 'utf8';

USE quorehotel;

CREATE TABLE IF NOT EXISTS stay (
  id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Quore Stay Id',
  quoreapi_api_signers_id INT(11) NOT NULL COMMENT 'API Signer (integrator) Id',
  property_id INT(11) NOT NULL COMMENT 'Property Id',
  reservation_id VARCHAR(255) DEFAULT NULL COMMENT 'PMS Resevation Id',
  booking_confirmation_id VARCHAR(255) DEFAULT NULL COMMENT 'PMS Booking Id',
  stay_group_id VARCHAR(255) DEFAULT NULL COMMENT 'When Stay is part of a Group',
  reference_number VARCHAR(255) DEFAULT NULL COMMENT 'PMS Booking,Stay Reference Id,Number',
  posted_on DATETIME DEFAULT CURRENT_TIMESTAMP COMMENT 'Date of Creation of Stay, Reservation, Booking',
  cancelled_on DATETIME DEFAULT NULL COMMENT 'Date of Cancelation of Stay, Reservation, Booking',
  cancellation_number VARCHAR(255) DEFAULT NULL COMMENT 'PMS Cancellation Number',
  updated_on DATETIME DEFAULT NULL COMMENT 'Last Update',
  quoreapi_source_id INT(11) DEFAULT NULL,
  source_info JSON DEFAULT NULL,
  comments TEXT DEFAULT NULL COMMENT 'Any Comments from PMS or Quore',
  PRIMARY KEY (id)
)
ENGINE = INNODB,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_general_ci,
COMMENT = 'Bookings or Reservations from Integrator';

ALTER TABLE stay 
  ADD INDEX `idx.stay.booking_confirmation_id`(booking_confirmation_id);

ALTER TABLE stay 
  ADD INDEX `idx.stay.cancellation_number`(cancellation_number);

ALTER TABLE stay 
  ADD INDEX `idx.stay.reference_number`(reference_number);

ALTER TABLE stay 
  ADD INDEX `idx.stay.reservation_id`(reservation_id);

ALTER TABLE stay 
  ADD INDEX `idx.stay.stay_group_id`(stay_group_id);

ALTER TABLE stay 
  ADD CONSTRAINT `fk.stay.property` FOREIGN KEY (property_id)
    REFERENCES property(id) ON UPDATE CASCADE;

ALTER TABLE stay 
  ADD CONSTRAINT `fk.stay.quoreapi.api_signers` FOREIGN KEY (quoreapi_api_signers_id)
    REFERENCES quoreapi.api_signers(id) ON UPDATE CASCADE;

ALTER TABLE stay 
  ADD CONSTRAINT `fk.stay.quoreapi_source` FOREIGN KEY (quoreapi_source_id)
    REFERENCES quoreapi.source(id) ON UPDATE CASCADE;

CREATE TABLE IF NOT EXISTS stay_guest (
  id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Stay Guest Id',
  stay_id INT(11) UNSIGNED DEFAULT NULL COMMENT 'Stay,Reservation,Booking Id',
  quoreapi_user_id INT(11) DEFAULT NULL COMMENT 'API User (Guest) Id',
  stay_room_id INT(11) UNSIGNED DEFAULT NULL COMMENT 'Stay Room Id',
  is_main_guest TINYINT(1) UNSIGNED DEFAULT 0 COMMENT 'If this Guest is the Main Guest (Stay Owner) on Stay,Reservation,Booking',
  posted_on DATETIME DEFAULT CURRENT_TIMESTAMP COMMENT 'When Guest was included in Room',
  deleted_on DATETIME DEFAULT NULL COMMENT 'When Guest was removed from Room',
  updated_on DATETIME DEFAULT NULL COMMENT 'Last Update',
  PRIMARY KEY (id)
)
ENGINE = INNODB,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_general_ci,
COMMENT = 'Guests allocated on Room for a Stay,Reservation,Booking';

ALTER TABLE stay_guest 
  ADD CONSTRAINT `fk.stay_guest.quoreapi.user` FOREIGN KEY (quoreapi_user_id)
    REFERENCES quoreapi.user(id) ON UPDATE CASCADE;

ALTER TABLE stay_guest 
  ADD CONSTRAINT `fk.stay_guest.stay` FOREIGN KEY (stay_id)
    REFERENCES stay(id) ON UPDATE CASCADE;

CREATE TABLE IF NOT EXISTS stay_room (
  id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  stay_id INT(11) UNSIGNED DEFAULT NULL COMMENT 'Stay Id',
  area_base_id INT(11) UNSIGNED DEFAULT NULL COMMENT 'Room Number,Id Mapping (if enecessary). Each Integrator,Property can have different room numbers comparing with Quore',
  room_rate_code VARCHAR(255) DEFAULT NULL COMMENT 'PMS Room Rate Code',
  room_block_code VARCHAR(255) DEFAULT NULL COMMENT 'PMS Room Block Code',
  posted_on DATETIME DEFAULT CURRENT_TIMESTAMP,
  arrival_date_time DATETIME DEFAULT NULL COMMENT 'Guests Arrival Date and Time (Local Time)',
  departure_date_time DATETIME DEFAULT NULL COMMENT 'Guests Arrival Date and Time (Local Time)',
  adults INT(3) DEFAULT 0,
  children INT(3) DEFAULT 0,
  infant INT(3) DEFAULT 0,
  updated_on DATETIME DEFAULT CURRENT_TIMESTAMP COMMENT 'Last Update',
  PRIMARY KEY (id)
)
ENGINE = INNODB,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_general_ci,
COMMENT = 'Room(s) assigned to a Stay,Reservation,Booking';

ALTER TABLE stay_room 
  ADD CONSTRAINT `fk.stay_room.area_base` FOREIGN KEY (area_base_id)
    REFERENCES area_base(id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE stay_room 
  ADD CONSTRAINT `fk.stay_room.stay` FOREIGN KEY (stay_id)
    REFERENCES stay(id) ON DELETE CASCADE ON UPDATE CASCADE;

CREATE TABLE IF NOT EXISTS area_status_stay (
  id CHAR(3) NOT NULL COMMENT 'Area Status Id (3 letters)',
  name VARCHAR(50) NOT NULL COMMENT 'Area Status Name',
  is_occupied TINYINT(1) UNSIGNED DEFAULT 0 COMMENT 'If this status makes rooms Occupied (if not, it should be Vacant)',
  active TINYINT(1) UNSIGNED DEFAULT 0 COMMENT 'If Area Status has been used',
  PRIMARY KEY (id)
)
ENGINE = INNODB,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_general_ci,
COMMENT = 'Area (Stay) Status (taxonomy)';

CREATE TABLE IF NOT EXISTS area_base_event_stay (
  id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Area Base Stay Event Id',
  area_base_id INT(11) UNSIGNED DEFAULT NULL COMMENT 'Area Id',
  area_stay_status_id CHAR(3) NOT NULL COMMENT 'Area Stay Status Id',
  posted_on DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Date/Time of event (UTC)',
  posted_by INT(11) DEFAULT NULL COMMENT 'User Id of who has posted event (if user)',
  stay_room_id INT(11) UNSIGNED DEFAULT NULL COMMENT 'Stay Room Id (if changed by Stay)',
  is_occupied TINYINT(1) DEFAULT NULL COMMENT 'when this status turns area into occuppied (maybe redundant - because area_stay_status has the same info)',
  PRIMARY KEY (id)
)
ENGINE = INNODB,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_general_ci,
COMMENT = 'Area Base Stay Event History';

ALTER TABLE area_base_event_stay 
  ADD CONSTRAINT `fk.area_base_event_stay.area_base` FOREIGN KEY (area_base_id)
    REFERENCES area_base(id) ON DELETE NO ACTION;

ALTER TABLE area_base_event_stay 
  ADD CONSTRAINT `fk.area_base_event_stay.area_stay_status` FOREIGN KEY (area_stay_status_id)
    REFERENCES area_status_stay(id) ON UPDATE CASCADE;

ALTER TABLE area_base_event_stay 
  ADD CONSTRAINT `fk.area_base_event_stay.stay_room` FOREIGN KEY (stay_room_id)
    REFERENCES stay_room(id) ON UPDATE CASCADE;

ALTER TABLE area_base_event_stay 
  ADD CONSTRAINT `fk.area_base_event_stay.user` FOREIGN KEY (posted_by)
    REFERENCES user(id) ON UPDATE CASCADE;

CREATE TABLE IF NOT EXISTS area_status_service (
  id CHAR(3) NOT NULL COMMENT 'Area Service Status Id (3 letters)',
  name VARCHAR(50) NOT NULL COMMENT 'Area Service Status Name',
  active TINYINT(1) UNSIGNED DEFAULT 0 COMMENT 'If Area Service Status it is in use',
  PRIMARY KEY (id)
)
ENGINE = INNODB,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_general_ci,
COMMENT = 'Area (Stay) Service Status (taxonomy)';

CREATE TABLE IF NOT EXISTS area_base_service_reason (
  id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Area Base Service Reason Id',
  name VARCHAR(50) NOT NULL COMMENT 'Area Base Service Reason Name',
  api_signers_id INT(11) DEFAULT NULL COMMENT 'Integrator Id (if service origininated on Integrator)',
  active TINYINT(1) UNSIGNED DEFAULT 1 COMMENT 'Is Active',
  PRIMARY KEY (id)
)
ENGINE = INNODB,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_general_ci,
COMMENT = 'Area Base Service Reasons (taxonomy)';

ALTER TABLE area_base_service_reason 
  ADD CONSTRAINT `fk.area_base_service_reason.quoreapi.api_signers` FOREIGN KEY (api_signers_id)
    REFERENCES quoreapi.api_signers(id) ON UPDATE CASCADE;

CREATE TABLE IF NOT EXISTS area_status_cleaning (
  id CHAR(3) NOT NULL COMMENT 'Area Cleaning Status Id (3 letters)',
  name VARCHAR(50) NOT NULL COMMENT 'Area Cleaning Status Name',
  active TINYINT(1) UNSIGNED DEFAULT 0 COMMENT 'If Area Cleaning Status it is in use',
  PRIMARY KEY (id)
)
ENGINE = INNODB,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_general_ci,
COMMENT = 'Area (Stay) Status Id (taxonomy)';

CREATE TABLE IF NOT EXISTS area_base_event_cleaning (
  id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Area Base Event Id',
  area_base_id INT(11) UNSIGNED DEFAULT NULL COMMENT 'Area Id',
  area_cleaning_status_id CHAR(3) NOT NULL COMMENT 'Area Cleaning Status Id',
  posted_on DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Date/Time of event (UTC)',
  posted_by INT(11) DEFAULT NULL COMMENT 'User Id of who has posted event (if user)',
  api_signers_id INT(11) DEFAULT NULL COMMENT 'Integrator Id of who has posted event (if integrator)',
  hk_board_room_id INT(10) UNSIGNED DEFAULT NULL COMMENT 'House Keeping Board Id (if changed by Hk Board)',
  PRIMARY KEY (id)
)
ENGINE = INNODB,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_general_ci,
COMMENT = 'Area Base Cleaning Event History';

ALTER TABLE area_base_event_cleaning 
  ADD CONSTRAINT `fk._area_base_event_cleaning.area_cleaning_status` FOREIGN KEY (area_cleaning_status_id)
    REFERENCES area_status_cleaning(id) ON UPDATE CASCADE;

ALTER TABLE area_base_event_cleaning 
  ADD CONSTRAINT `fk.area_base_event_cleaning.area_base` FOREIGN KEY (area_base_id)
    REFERENCES area_base(id) ON DELETE NO ACTION;

ALTER TABLE area_base_event_cleaning 
  ADD CONSTRAINT `fk.area_base_event_cleaning.hk_board_room` FOREIGN KEY (hk_board_room_id)
    REFERENCES hk_board_room(id) ON UPDATE CASCADE;

ALTER TABLE area_base_event_cleaning 
  ADD CONSTRAINT `fk.area_base_event_cleaning.quoreapi.api_signers` FOREIGN KEY (api_signers_id)
    REFERENCES quoreapi.api_signers(id) ON UPDATE CASCADE;

ALTER TABLE area_base_event_cleaning 
  ADD CONSTRAINT `fk.area_base_event_cleaning.user` FOREIGN KEY (posted_by)
    REFERENCES user(id) ON UPDATE CASCADE;

CREATE TABLE IF NOT EXISTS area_base_event_service (
  id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Area Base Service Event Id',
  area_base_id INT(11) UNSIGNED NOT NULL COMMENT 'Area Id',
  area_service_status_id CHAR(3) DEFAULT NULL,
  area_base_service_reason_id INT(11) UNSIGNED DEFAULT NULL COMMENT 'Area Base Service Reason Id (taxonomy)',
  posted_on DATETIME DEFAULT CURRENT_TIMESTAMP COMMENT 'Date/Time of event (UTC)',
  posted_by INT(11) DEFAULT NULL COMMENT 'User Id of who has posted event (if user)',
  start_date_time DATETIME NOT NULL COMMENT 'Start Date/Time of Service Event (Local Time)',
  end_date_time DATETIME NOT NULL COMMENT 'End Date/Time of Service Event (Local Time)',
  workorder_id INT(11) DEFAULT NULL COMMENT 'Workorder Id (if service status changed by a workorder)',
  insp_inspection_items INT(11) DEFAULT NULL COMMENT 'Inspection Item Id (if service status changed by a inspection)',
  notes TEXT DEFAULT NULL COMMENT 'Notes about Service',
  PRIMARY KEY (id)
)
ENGINE = INNODB,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_general_ci,
COMMENT = 'Area Base Service Event History';

ALTER TABLE area_base_event_service 
  ADD CONSTRAINT `fk.area_base_event_service.area_base` FOREIGN KEY (area_base_id)
    REFERENCES area_base(id) ON UPDATE CASCADE;

ALTER TABLE area_base_event_service 
  ADD CONSTRAINT `fk.area_base_event_service.area_base_service_reason` FOREIGN KEY (area_base_service_reason_id)
    REFERENCES area_base_service_reason(id) ON UPDATE CASCADE;

ALTER TABLE area_base_event_service 
  ADD CONSTRAINT `fk.area_base_event_service.area_service_status` FOREIGN KEY (area_service_status_id)
    REFERENCES area_status_service(id) ON UPDATE CASCADE;

ALTER TABLE area_base_event_service 
  ADD CONSTRAINT `fk.area_base_event_service.insp_inspection_items` FOREIGN KEY (insp_inspection_items)
    REFERENCES insp_inspection_items(id) ON UPDATE CASCADE;

ALTER TABLE area_base_event_service 
  ADD CONSTRAINT `fk.area_base_event_service.user` FOREIGN KEY (posted_by)
    REFERENCES user(id) ON UPDATE CASCADE;

ALTER TABLE area_base_event_service 
  ADD CONSTRAINT `fk.area_base_event_service.workorder` FOREIGN KEY (workorder_id)
    REFERENCES workorder(id) ON UPDATE CASCADE;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;