﻿--
-- Disable foreign keys
--
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

SET NAMES 'utf8';

--
-- Set default database
--
USE quorehotel;

--
-- Create table `area_status`
--
CREATE TABLE IF NOT EXISTS area_status (
  id CHAR(3) NOT NULL COMMENT 'Area Status Id (3 letters)',
  name VARCHAR(50) NOT NULL COMMENT 'Area Status Name',
  is_vacant TINYINT(1) UNSIGNED DEFAULT 0 COMMENT 'If this status makes rooms Vacant',
  is_occupied TINYINT(1) UNSIGNED DEFAULT 0 COMMENT 'If this status makes rooms Occupied',
  active TINYINT(1) UNSIGNED DEFAULT 0 COMMENT 'If Area Status has been used',
  PRIMARY KEY (id)
)
ENGINE = INNODB,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_general_ci,
COMMENT = 'Area (Stay) Status (taxonomy)';

--
-- Create table `stay`
--
CREATE TABLE IF NOT EXISTS stay (
  id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Quore Stay Id',
  quoreapi_api_signers_id INT(11) NOT NULL COMMENT 'API Signer (integrator) Id',
  property_id INT(11) NOT NULL COMMENT 'Property Id',
  reservation_id VARCHAR(255) DEFAULT NULL COMMENT 'PMS Resevation Id',
  booking_confirmation_id VARCHAR(255) DEFAULT NULL COMMENT 'PMS Booking Id',
  stay_group_id VARCHAR(255) DEFAULT NULL COMMENT 'When Stay is part of a Group',
  reference_number VARCHAR(255) DEFAULT NULL COMMENT 'PMS Booking,Stay Reference Id,Number',
  area_status_id CHAR(3) DEFAULT NULL COMMENT 'Stay Status Id (3 letters) (grouped calculated by stayRoom(s) - it should be a Reservation,Booking when there are no rooms with "Checked In" status)',
  posted_on DATETIME DEFAULT NULL COMMENT 'Date of Creation of Stay, Reservation, Booking',
  cancelled_on DATETIME DEFAULT NULL COMMENT 'Date of Cancelation of Stay, Reservation, Booking',
  cancellation_number VARCHAR(255) DEFAULT NULL COMMENT 'PMS Cancellation Number',
  comments TEXT DEFAULT NULL COMMENT 'Any Comments from PMS or Quore',
  inserted_on DATETIME DEFAULT CURRENT_TIMESTAMP,
  quoreapi_source_id INT(11) DEFAULT NULL,
  source_info JSON DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_general_ci,
COMMENT = 'Bookings or Reservations from Integrator';

--
-- Create index `fk`.`stay`.`property` on table `stay`
--
ALTER TABLE stay 
  ADD INDEX `fk.stay.property`(property_id);

--
-- Create index `idx`.`stay`.`bookingConfirmationId` on table `stay`
--
ALTER TABLE stay 
  ADD INDEX `idx.stay.bookingConfirmationId`(booking_confirmation_id);

--
-- Create index `idx`.`stay`.`cancellationNumber` on table `stay`
--
ALTER TABLE stay 
  ADD INDEX `idx.stay.cancellationNumber`(cancellation_number);

--
-- Create index `idx`.`stay`.`referenceNumber` on table `stay`
--
ALTER TABLE stay 
  ADD INDEX `idx.stay.referenceNumber`(reference_number);

--
-- Create index `idx`.`stay`.`reservationId` on table `stay`
--
ALTER TABLE stay 
  ADD INDEX `idx.stay.reservationId`(reservation_id);

--
-- Create index `idx`.`stay`.`stayGroupId` on table `stay`
--
ALTER TABLE stay 
  ADD INDEX `idx.stay.stayGroupId`(stay_group_id);

--
-- Create foreign key
--
ALTER TABLE stay 
  ADD CONSTRAINT `fk.stay.area_status` FOREIGN KEY (area_status_id)
    REFERENCES area_status(id) ON UPDATE CASCADE;

--
-- Create foreign key
--
ALTER TABLE stay 
  ADD CONSTRAINT `fk.stay.quoreapi.api_signers` FOREIGN KEY (quoreapi_api_signers_id)
    REFERENCES quoreapi.api_signers(id) ON UPDATE CASCADE;

--
-- Create table `area_service_status`
--
CREATE TABLE IF NOT EXISTS area_service_status (
  id CHAR(3) NOT NULL COMMENT 'Area Service Status Id (3 letters)',
  name VARCHAR(50) NOT NULL COMMENT 'Area Service Status Name',
  active TINYINT(1) UNSIGNED DEFAULT 0 COMMENT 'If Area Service Status it is in use',
  PRIMARY KEY (id)
)
ENGINE = INNODB,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_general_ci,
COMMENT = 'Area (Stay) Service Status (taxonomy)';

--
-- Create table `area_cleaning_status`
--
CREATE TABLE IF NOT EXISTS area_cleaning_status (
  id CHAR(3) NOT NULL COMMENT 'Area Cleaning Status Id (3 letters)',
  name VARCHAR(50) NOT NULL COMMENT 'Area Cleaning Status Name',
  active TINYINT(1) UNSIGNED DEFAULT 0 COMMENT 'If Area Cleaning Status it is in use',
  PRIMARY KEY (id)
)
ENGINE = INNODB,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_general_ci,
COMMENT = 'Area (Stay) Status Id (taxonomy)';

--
-- Create table `stay_room`
--
CREATE TABLE IF NOT EXISTS stay_room (
  id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  stay_id INT(11) UNSIGNED DEFAULT NULL COMMENT 'Stay Id',
  area_base_id INT(11) UNSIGNED DEFAULT NULL COMMENT 'Room Number,Id Mapping (if enecessary). Each Integrator,Property can have different room numbers comparing with Quore',
  area_status_id CHAR(3) NOT NULL COMMENT 'Quore Area Status Id',
  area_service_status_id CHAR(3) DEFAULT NULL COMMENT 'Area Service Status Id (3 letters)',
  area_cleaning_status_id CHAR(3) NOT NULL COMMENT 'Quore Area Cleaning Status Id (3 letters)',
  room_type VARCHAR(255) DEFAULT NULL COMMENT 'PMS Room type',
  room_rate_code VARCHAR(255) DEFAULT NULL COMMENT 'PMS Room Rate Code',
  room_block_code VARCHAR(255) DEFAULT NULL COMMENT 'PMS Room Block Code',
  posted_on DATETIME DEFAULT CURRENT_TIMESTAMP,
  arrival_date DATETIME DEFAULT NULL COMMENT 'Guests Arrival Date and Time (Local Time)',
  departure_date DATETIME DEFAULT NULL COMMENT 'Guests Arrival Date and Time (Local Time)',
  adults INT(3) DEFAULT 0,
  children INT(3) DEFAULT 0,
  infant INT(3) DEFAULT 0,
  inserted_on DATETIME DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_general_ci,
COMMENT = 'Room(s) assigned to a Stay,Reservation,Booking';

--
-- Create index `idx`.`stay_room`.`area_cleaning_status` on table `stay_room`
--
ALTER TABLE stay_room 
  ADD INDEX `idx.stay_room.area_cleaning_status`(area_cleaning_status_id);

--
-- Create index `idx`.`stay_room`.`area_status` on table `stay_room`
--
ALTER TABLE stay_room 
  ADD INDEX `idx.stay_room.area_status`(area_status_id);

--
-- Create foreign key
--
ALTER TABLE stay_room 
  ADD CONSTRAINT `fk.stay_room.area_base` FOREIGN KEY (area_base_id)
    REFERENCES area_base(id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Create foreign key
--
ALTER TABLE stay_room 
  ADD CONSTRAINT `fk.stay_room.area_cleaning_status` FOREIGN KEY (area_cleaning_status_id)
    REFERENCES area_cleaning_status(id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Create foreign key
--
ALTER TABLE stay_room 
  ADD CONSTRAINT `fk.stay_room.area_service_status` FOREIGN KEY (area_service_status_id)
    REFERENCES area_service_status(id) ON UPDATE CASCADE;

--
-- Create foreign key
--
ALTER TABLE stay_room 
  ADD CONSTRAINT `fk.stay_room.area_status` FOREIGN KEY (area_status_id)
    REFERENCES area_status(id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Create foreign key
--
ALTER TABLE stay_room 
  ADD CONSTRAINT `fk.stay_room.stay` FOREIGN KEY (stay_id)
    REFERENCES stay(id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Create table `stay_guest`
--
CREATE TABLE IF NOT EXISTS stay_guest (
  stay_id INT(11) UNSIGNED NOT NULL COMMENT 'Stay,Reservation,Booking Id',
  quoreapi_user_id INT(11) NOT NULL COMMENT 'API User (Guest) Id',
  stay_room_id INT(11) UNSIGNED DEFAULT NULL COMMENT 'Stay Room Id',
  is_main_guest TINYINT(1) UNSIGNED DEFAULT 0 COMMENT 'If this Guest is the Main Guest (Stay Owner) on Stay,Reservation,Booking',
  posted_on DATETIME DEFAULT NULL COMMENT 'When Guest was included in Room',
  deleted_on DATETIME DEFAULT NULL COMMENT 'When Guest was removed from Room',
  inserted_on DATETIME DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (stay_id, quoreapi_user_id)
)
ENGINE = INNODB,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_general_ci,
COMMENT = 'Guests allocated on Room for a Stay,Reservation,Booking';

--
-- Create foreign key
--
ALTER TABLE stay_guest 
  ADD CONSTRAINT `fk.stay_guest.quoreapi.user` FOREIGN KEY (quoreapi_user_id)
    REFERENCES quoreapi.user(id) ON UPDATE CASCADE;

--
-- Create foreign key
--
ALTER TABLE stay_guest 
  ADD CONSTRAINT `fk.stay_guest.stay` FOREIGN KEY (stay_id)
    REFERENCES stay(id) ON UPDATE CASCADE;

--
-- Enable foreign keys
--
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;

SET NAMES 'utf8';
INSERT INTO quorehotel.area_cleaning_status(id, name, active) VALUES ('CLN', 'Clean', 1);
INSERT INTO quorehotel.area_cleaning_status(id, name, active) VALUES ('DRT', 'Dirty', 1);
INSERT INTO quorehotel.area_cleaning_status(id, name, active) VALUES ('INS', 'Inspected', 1);
INSERT INTO quorehotel.area_cleaning_status(id, name, active) VALUES ('PIC', 'Pickup', 1);
INSERT INTO quorehotel.area_cleaning_status(id, name, active) VALUES ('UNK', 'Unknown', 1);

INSERT INTO quorehotel.area_service_status(id, name, active) VALUES ('OK', 'In Service', 1);
INSERT INTO quorehotel.area_service_status(id, name, active) VALUES ('OOI', 'Out Of Inventory', 1);
INSERT INTO quorehotel.area_service_status(id, name, active) VALUES ('OOO', 'Out Of Order', 1);
INSERT INTO quorehotel.area_service_status(id, name, active) VALUES ('OOS', 'Out Of Service', 1);

INSERT INTO quorehotel.area_status(id, name, is_vacant, is_occupied, active) VALUES ('IN', 'Checked In', 0, 1, 1);
INSERT INTO quorehotel.area_status(id, name, is_vacant, is_occupied, active) VALUES ('INH', 'In House', 1, 0, 1);
INSERT INTO quorehotel.area_status(id, name, is_vacant, is_occupied, active) VALUES ('LST', 'Waiting List', 1, 0, 1);
INSERT INTO quorehotel.area_status(id, name, is_vacant, is_occupied, active) VALUES ('NSH', 'No Show', 1, 0, 1);
INSERT INTO quorehotel.area_status(id, name, is_vacant, is_occupied, active) VALUES ('OUT', 'Checked Out', 1, 0, 1);
INSERT INTO quorehotel.area_status(id, name, is_vacant, is_occupied, active) VALUES ('PRE', 'Pre Check-In', 1, 0, 1);
INSERT INTO quorehotel.area_status(id, name, is_vacant, is_occupied, active) VALUES ('REQ', 'Requested', 1, 0, 1);
INSERT INTO quorehotel.area_status(id, name, is_vacant, is_occupied, active) VALUES ('RES', 'Reserved', 1, 0, 1);
INSERT INTO quorehotel.area_status(id, name, is_vacant, is_occupied, active) VALUES ('TRF', 'Transfered', 1, 0, 1);
INSERT INTO quorehotel.area_status(id, name, is_vacant, is_occupied, active) VALUES ('UNK', 'Unknown', 0, 0, 1);
INSERT INTO quorehotel.area_status(id, name, is_vacant, is_occupied, active) VALUES ('X', 'Cancelled', 1, 0, 1);