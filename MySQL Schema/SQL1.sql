﻿-- 
-- Set character set the client will use to send SQL statements to the server
--
SET NAMES 'utf8';

--
-- Set default database
--
USE quoreapi;

DELIMITER $$

--
-- Create procedure `StayNTouch_GetGuests`
--
CREATE PROCEDURE StayNTouch_GetAccounts (IN IN_Accounts json)
NO SQL
COMMENT 'Read JSON from result of StayNTouch API call to "getGuests"'
BEGIN
	DROP TABLE IF EXISTS sntAccounts;
	CREATE TEMPORARY TABLE IF NOT EXISTS sntAccounts
	SELECT _json_int_cleaner(IN_Accounts, CONCAT('results[', sequence.seq, '].id'))						 AS `id`,
		   _json_int_cleaner(IN_Accounts, CONCAT('results[', sequence.seq, '].hotel_id'))			     AS `hotel_id`,
		   _json_char_cleaner(IN_Accounts, CONCAT('results[', sequence.seq, '].account_number'))		 AS `account_number`,
		   _json_char_cleaner(IN_Accounts, CONCAT('results[', sequence.seq, '].type'))				     AS `type`,
		   _json_char_cleaner(IN_Accounts, CONCAT('results[', sequence.seq, '].name'))					 AS `name`,

		   _json_int_cleaner(IN_Accounts, CONCAT('results[', sequence.seq, '].address.id'))				 AS `address.id`,
		   _json_char_cleaner(IN_Accounts, CONCAT('results[', sequence.seq, '].address.address_line1'))	 AS `address.address_line1`,
		   _json_char_cleaner(IN_Accounts, CONCAT('results[', sequence.seq, '].address.address_line2'))	 AS `address.address_line2`,
		   _json_char_cleaner(IN_Accounts, CONCAT('results[', sequence.seq, '].address.city'))			 AS `address.city`,
		   _json_char_cleaner(IN_Accounts, CONCAT('results[', sequence.seq, '].address.state'))			 AS `address.state`,
		   _json_char_cleaner(IN_Accounts, CONCAT('results[', sequence.seq, '].address.postal_code'))	 AS `address.postal_code`,
		   _json_char_cleaner(IN_Accounts, CONCAT('results[', sequence.seq, '].address.country_code'))	 AS `address.country_code`,

		   _json_char_cleaner(IN_Accounts, CONCAT('results[', sequence.seq, '].ar_number'))				 AS `ar_number`,
		   _json_bool_cleaner(IN_Accounts, CONCAT('results[', sequence.seq, '].is_global')) 			 AS `is_global`,
		   _json_char_cleaner(IN_Accounts, CONCAT('results[', sequence.seq, '].tax_number'))		     AS `tax_number`
	FROM sequence
	WHERE seq < JSON_LENGTH(JSON_EXTRACT(IN_Accounts, '$.results'));
END
$$

DELIMITER ;