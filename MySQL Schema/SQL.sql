﻿CREATE PROCEDURE quorechestration.StayNTouch_Init_Guests (IN IN_Guests text, IN IN_HotelId int, IN IN_Verbose tinyint(1), OUT OUT_Response json)
SQL SECURITY INVOKER
MODIFIES SQL DATA
BEGIN
	-- Verbose support variables 
	DECLARE TimerStart decimal(16, 6) DEFAULT UNIX_TIMESTAMP(NOW(6));
	DECLARE ActionStart decimal(16, 6);
	DECLARE OperationResponses json;
	DECLARE Action text;
	DECLARE CallResult json;
	DECLARE Request json DEFAULT JSON_OBJECT('guests', IN_Guests, 'hotel_id', IN_HotelId);

	-- Error Handlers variables
	DECLARE ErrorCode int(11);
	DECLARE ErrorMsg text;
	DECLARE DiagNumber int(11);
	DECLARE DiagRows int(11);

	-- Internal Variables
	DECLARE NumberOfGuests int(11);
	DECLARE RecordCount int(11);
	DECLARE RecordFound tinyint(1);

	DECLARE LastInsert int(11);
	DECLARE MapPropertyId int(11);
	DECLARE PropertyId int(11);

	-- Declare exception handler for failures
	DECLARE EXIT HANDLER FOR SQLEXCEPTION, SQLWARNING, SQLSTATE '45000', SQLSTATE '45001' BEGIN
		GET DIAGNOSTICS CONDITION 1
		ErrorCode = RETURNED_SQLSTATE,
		ErrorMsg = MESSAGE_TEXT;

		SET OperationResponses = _AddErrorToOperationResponses(OperationResponses, Action, ErrorMsg, ErrorCode, TRUE, TimerStart);
		SET Out_Response = _BuildResponse(IN_Verbose, 'fail', OperationResponses, Request, JSON_OBJECT('message', ErrorMsg, 'code', ErrorCode), TimerStart);

		ROLLBACK;
	END;

	DECLARE CONTINUE HANDLER FOR NOT FOUND
		SET RecordFound = 0;

	GET DIAGNOSTICS DiagNumber = NUMBER, DiagRows = ROW_COUNT;

-- Verify if IN_Guests are valid
ValidateInData:
	BEGIN
		SET Action = 'data-validation';
		SET ActionStart = UNIX_TIMESTAMP(NOW(6));

		SET OperationResponses = _AddActionToOperationResponses(OperationResponses, Action, 'step', 'Data Validated', ActionStart);

		IF (ISNULL(IN_Guests)
			OR TRIM(IN_Guests) = '') THEN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Guests Data required!';
		ELSEIF (!JSON_VALID(IN_Guests)) THEN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Guests Data!';
		END IF;
	END ValidateInData;

MappingGuests:
	BEGIN
		START TRANSACTION;

		SET Action = 'insert-mapping';
		SET ActionStart = UNIX_TIMESTAMP(NOW(6));

		CALL StayNTouch_GetGuests(IN_Guests); -- Table: sntGuests

		SELECT
			COUNT(*)
		INTO NumberOfGuests
		FROM sntGuests;
		
		SET RecordCount = 0;
		WHILE (RecordCount < NumberOfGuests) DO
			SELECT
				@guestId := id,
				@guestFirstName := first_name,
				@guestLastName := last_name
			FROM sntGuests
			LIMIT RecordCount, 1;

			SET RecordFound = 1;

			SELECT
				@currentUserId := user_id
			FROM quoreapi.api_signers_map_user
			WHERE api_signers_id = 200
			AND signer_guest_id = @guestId;

			IF (@currentUserId IS NULL) THEN BEGIN
					INSERT INTO quoreapi.user (first_name, last_name, created_on)
						VALUES (@guestFirstName, @guestLastName, CURRENT_TIMESTAMP());
			
					INSERT INTO quoreapi.api_signers_map_user (api_signers_id, signer_guest_id, user_id)
						VALUES (200, @guestId, LAST_INSERT_ID());
				END;
			ELSE
				UPDATE quoreapi.user
				SET first_name = @guestFirstName,
					last_name = @guestLastName
				WHERE id = @currentUserId;
			END IF;
	   	END WHILE;

		COMMIT;

		SET CallResult = JSON_OBJECT('guests', NumberOfGuests);
		SET OperationResponses = _AddActionToOperationResponses(OperationResponses, Action, 'process', 'Guests Mapping Inserted', ActionStart);
	END MappingGuests;

	-- Final response
	SET Out_Response = _BuildResponse(IN_Verbose, 'success', OperationResponses, Request, CallResult, TimerStart);
END