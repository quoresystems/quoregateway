﻿CREATE PROCEDURE quoreapi.addOperation(IN _api_signers_endpoint_id INT(11), IN _event VARCHAR(50), IN _data JSON)
  SQL SECURITY INVOKER
  READS SQL DATA
  COMMENT 'Add records to operation'
BEGIN
DECLARE resultMessages JSON;

-- Declare exception handler for failures
DECLARE EXIT HANDLER FOR SQLEXCEPTION, SQLWARNING, SQLSTATE '45000' BEGIN
    GET DIAGNOSTICS CONDITION 1
    @errorCode = RETURNED_SQLSTATE,
    @errorMsg = MESSAGE_TEXT;

    SET resultMessages = JSON_ARRAY_APPEND(resultMessages, '$', JSON_OBJECT('type', 'error', 'text', @ERRORMSG, 'code', @errorCode, 'step', @debugStep));

    SELECT
        JSON_OBJECT('result', 'ok', 'messages', resultMessages, 'response', @postProcessResponse) AS response;
END;

SET resultMessages = JSON_ARRAY();

-- Declare variables to hold diagnostics area information
GET DIAGNOSTICS
@diagNumber = NUMBER,
@diagRows = ROW_COUNT;

-- Declare variables to hold debug information
SET @debugStep = '';

# ----------------------------------------------------
Validations: BEGIN
    SET @debugStep = 'Initial Validation';
    IF (ISNULL(_api_signers_endpoint_id) OR _api_signers_endpoint_id = 0) THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'API Signer (Integrator) Id required!';
    ELSEIF (ISNULL(_event) OR TRIM(_event) = '') THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Source required!';
    ELSEIF (ISNULL(_data) OR TRIM(_data) = '') THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Data required!';
    ELSEIF (!JSON_VALID(_data)) THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data!';
    END IF;
END Validations;

# ----------------------------------------------------
Main: BEGIN

    # ----------------------------------------------------
    SET @debugStep = 'Insert operation';
    InsertOperation: BEGIN
        INSERT INTO api_signers_endpoint_operation (api_signers_endpoint_id, event, data)
            VALUES (_api_signers_endpoint_id, _event, _data);

        SET @newId = LAST_INSERT_ID();
        SET resultMessages = JSON_ARRAY_APPEND(resultMessages, '$', JSON_OBJECT('type', 'step', 'text', CONCAT('Operation Inserted (id:', @newId, ')'), 'code', NULL, 'step', @debugStep));
    END InsertOperation;

    # ----------------------------------------------------
    PostProcessCall: BEGIN
        SET @debugStep = 'Post Process Call';
    
        ## TODO
    
        SET @postProcessResponse = NULL;

        SET resultMessages = JSON_ARRAY_APPEND(resultMessages, '$', JSON_OBJECT('type', 'step', 'text', @postProcessResponse, 'code', NULL, 'step', @debugStep));
    END PostProcessCall;

    # ----------------------------------------------------
    UpdateOperationWithResponse: BEGIN
        SET @debugStep = 'Update operation';

        UPDATE api_signers_endpoint_operation 
            SET after_process_response = @postProcessResponse, after_process_response_date = NOW() 
        WHERE id = @newId;

        SET resultMessages = JSON_ARRAY_APPEND(resultMessages, '$', JSON_OBJECT('type', 'step', 'text', 'Operation Updated', 'code', NULL, 'step', @debugStep));
    END UpdateOperationWithResponse;

END Main;

SELECT JSON_OBJECT('result', 'ok', 'messages', resultMessages, 'response', @postProcessResponse) AS response;

END