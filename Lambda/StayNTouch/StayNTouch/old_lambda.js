﻿console.log('Loading function');
const doc = require('dynamodb-doc');
const mysql = require('rds-config');
const dynamo = new doc.DynamoDB();

var thisUUID = create_UUID();
var spResult = '{ "result": "---" }';

function create_UUID() {
    var dt = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (dt + Math.random() * 16) % 16 | 0;
        dt = Math.floor(dt / 16);
        return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
    return uuid;
}

exports.handler = (event, context, callback) => {
    console.log('Received event:', JSON.stringify(event, null, 2));

    const done = (err, res) => callback(null, {
        statusCode: err ? '400' : '200',
        body: err ? err.message : '{ "response": { "eventId": "' + thisUUID + '", ' + JSON.stringify(spResult) + ' } }',
        headers: {
            'Content-Type': 'application/json',
        },
    });

    switch (event.httpMethod) {
        case 'POST':
            /*
            TODO: CALL SP 
            */

            var connection = mysql.createConnection({
                host: 'quore-cluster.cluster-cmwtqs8fk9hg.us-east-1.rds.amazonaws.com',
                user: 'root',
                password: 'Paiva#3815',
                database: 'quoreapi'
            });
            console.log(connection);

            var params = {};
            params.TableName = "quoreGateway";
            params.Item = { "eventId": thisUUID, "event": event.body };

            dynamo.putItem(params, done);
            break;
        default:
            done(new Error(`Unsupported method "${event.httpMethod}"`));
    }
};
