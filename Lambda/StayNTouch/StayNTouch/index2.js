﻿"use strict";
console.log("Loading");

const config = require("./config.json");

//var fs = require("fs");
//var event = JSON.parse(fs.readFileSync("_eventSample.json", "utf8").trim());

const mysql = require('serverless-mysql')({
    config: {
        host: config.dbhost,
        database: config.dbname,
        user: config.dbuser,
        password: config.dbpassword
    }
});

exports.handler = async function (payload, context) {
    context.callbackWaitsForEmptyEventLoop = false;

    var statusCode;
    var result = "";
    var action = "";

    console.log("Received payload:", JSON.stringify(payload, null, 2));

    if (payload.body) {
        payload = payload.body;
    }

    switch (payload.event) {
        case "checkin_available":
            action = "ProcessAvailableCheckIn";

            // Run your query
            let queryResult = await mysql.query(`CALL addPMSEvent(1, '${action}', '${JSON.stringify(payload.event.data)}', NULL)`);

            // Run clean up function
            await mysql.end();

            statusCode = 200;
            result = {
                "status": "success",
                "response": {
                    "message": queryResult,
                    "code": ""
                }
            };
            break;

        default:
            statusCode = 400;
            result = {
                "status": "fail",
                "response": {
                    "message": "Unsupported event = " + payload.event,
                    "code": ""
                }
            };
    }

    const response = {
        statusCode: statusCode,
        headers: { "Content-type": "application/json" },
        body: JSON.stringify(result)
    };

    return response;
};
