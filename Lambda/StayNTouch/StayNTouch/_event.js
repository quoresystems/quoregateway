﻿var fs = require('fs');
var app = require('./app');

var event = JSON.parse(fs.readFileSync('_eventSample.json', 'utf8').trim());

var context = {};

app.handler(event, context, callback) = function (event, context, callback) {
    callback(null, 'ok');
};

context.done = function () {
    console.log("Lambda Function Complete");
};

