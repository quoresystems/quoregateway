﻿console.debug('Loading');

const config = require('./config.json');
const mysql = require('mysql');

var response = null;
var action = "";

const pool = mysql.createPool({
    host: config.dbhost,
    user: config.dbuser,
    password: config.dbpassword,
    database: config.dbname
});

exports.handler = function (event, context, callback) {
    console.log('Received event:', JSON.stringify(event, null, 2));
    console.log('event =', event.event);
    console.log('data =', event.data);

    var result = MyLambdaFunction(event);

    console.log('result =', result);
    callback(null, result);
};

async function MyLambdaFunction(event) {
    var result = {};

    switch (event.httpMethod) {
        case 'POST':
            if (event.event === "checkin_available") {
                action = "ProcessAvailableCheckIn";
            }

            await pool.getConnection(function (connectionError, connection) {
                if (connectionError) {
                    console.debug("MySql Connection Error: " + connectionError);
                    result = {
                        'status': 'fail', 'response': { "message": connectionError.message, "code": connectionError.code }
                    };
                } else {
                    var query = `CALL addPMSEvent(1, '${action}', '${JSON.stringify(event.data)}', NULL)`;
                    console.debug(`Query: ${query}`);

                    var queryResult = pool.query(query);

                    console.debug(`Query Result: ${queryResult}`);

                    connection.release();
                }
            });

            break;
        default:
            result = {
                'status': 'fail', 'response': { "message": `Unsupported method "${event.httpMethod}"`, "code": '' }
            };
    }

    return result;
}
