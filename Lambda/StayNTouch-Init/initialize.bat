node index.js --route hotels/290 --action InitializeStayNTouchHotel
node index.js --route hotels/290/room_types --action InitializeStayNTouchRoomTypes
node index.js --route hotels/290/rooms?page=1&per_page=1000 --action InitializeStayNTouchRooms
node index.js --route reservations?page=1&per_page=10&hotel_id=290&date_filter=arrival&date_operator=gte&date=2019-01-01 --action InitializeStayNTouchReservations