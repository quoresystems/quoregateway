﻿var config = require('./config.json');
var argv = require('minimist')(process.argv.slice(2));

const request = require('sync-request');
const mysql = require('mysql');

const options = {
    headers: { 
        'API-Version': '2.0', 
        'Authorization': 'Bearer ' + config.sntAccessToken, 
        'Content-Type': 'application/json', 
        'cache-control': 'no-cache'
    }
};

var errors = false;
if (!argv.route) {
    console.log('Missing parameter `route`. Ex.: `hotels/290');
    errors = true;
}

if (!argv.action) {
    console.log('Missing parameter `action`. Ex.: `InitializeStayNTouchHotel');
    errors = true;
}

if (!errors) {
    const uri = config.sntConnectUrl + argv.route;
    const action = argv.action;

    const pool = mysql.createPool({
        host     : config.dbhost,
        user     : config.dbuser,
        password : config.dbpassword,
        database : config.dbname
    });
    
    console.log("\nProcessing " + action + "\n");
    
    try {
        var response = request('GET', uri, options).getBody('UTF-8');
    
        pool.getConnection(function(connectionError, connection) {
            if (connectionError) {
                console.log("MySql Connection Error: " + connectionError);
            } else {
                let query = `CALL addOperation(1, '${action}', '${response}')`;
    
                connection.query(query, 
                    function (queryError, results, fields) {
                        connection.release();
            
                        console.log(query + "\n");
                        if (queryError) {
                            console.log("Query Error: " + queryError);
                        } else {
                            console.log("Result:");
                            console.log(JSON.stringify(results));
                        }

                        process.exit();
                    }
                );
            }
        });
    } catch (error) {
        console.log("Response Error: " + error);
    }
}

// exports.handler = (event, context, callback) => {
//     context.callbackWaitsForEmptyEventLoop = false;
//     pool.getConnection(function(err, connection) {
//         connection.query('SELECT * FROM api_signers', function (error, results, fields) {
//             connection.release();

//             if (error) {
//                 callback(error);
//             } else {
//                 callback(null, results);
//             }
//         });
//     });
// };